-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2021 at 05:21 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `noithat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp(),
  `islogin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`, `email`, `phone`, `created`, `islogin`) VALUES
(1, 'Quản trị hệ thống', 'admin1', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0949189255', '2019-07-19 16:18:15', 1),
(2, 'Quản trị hệ thống', 'admin2', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0909908763', '2019-07-19 16:19:47', 0),
(6, 'Quản trị hệ thống', 'admin3', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0990909092', '2019-07-21 15:46:53', 0),
(7, 'Quản trị hệ thống', 'admin4', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0990909091', '2019-11-10 11:14:33', 0),
(8, 'Quản trị hệ thống', 'admin5', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0949189255', '2019-11-10 11:20:06', 0),
(9, 'Quản trị hệ thống', 'admin6', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0909908763', '2019-12-08 14:36:00', 0),
(10, 'Quản trị hệ thống', 'admin7', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0909908761', '2019-12-08 14:36:32', 0),
(11, 'Quản trị hệ thống', 'admin8', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0990909091', '2019-12-08 14:37:06', 0),
(12, 'Quản trị hệ thống', 'admin9', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0999999999', '2019-12-08 14:37:53', 0),
(13, 'Quản trị hệ thống', 'admin0', '7c222fb2927d828af22f592134e8932480637c0d', 'admin@gmail.com', '0990909092', '2019-12-08 14:38:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_bin NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `name`, `parent_id`, `note`, `created`) VALUES
(1, 'Ghế', 0, 'Ghế', '2020-10-08 21:20:26'),
(2, 'Ghế văn phòng', 1, 'Ghế văn phòng', '2020-10-08 21:20:41'),
(3, 'Bàn', 0, 'Bàn', '2020-10-08 21:36:32'),
(4, 'Bàn ăn', 3, 'Bàn ăn', '2020-10-08 21:39:22'),
(5, 'Tủ', 0, 'Tủ', '2020-10-08 21:40:06'),
(6, 'Tủ bếp', 5, 'Tủ bếp', '2020-10-08 21:40:36'),
(7, 'Bàn giám đốc', 3, 'Bàn giám đốc', '2020-10-08 22:39:19'),
(8, 'Bàn họp', 3, 'Bàn họp', '2020-10-08 22:41:33'),
(9, 'Sofa phòng khách', 0, 'Sofa phòng khách', '2020-10-08 22:43:09'),
(10, 'Cửa gỗ', 0, 'Cửa gỗ', '2020-10-08 22:43:31'),
(11, 'Giường', 0, 'Giường ngủ', '2020-10-15 21:53:23'),
(12, 'Phòng ngủ', 0, 'Phòng ngủ', '2020-10-18 11:38:00'),
(13, 'Tủ áo', 5, 'Tủ áo', '2020-10-18 13:25:25'),
(14, 'Kệ Tivi', 0, 'Kệ Tivi', '2020-10-24 10:22:02');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `user_email` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `messsage` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `design_catalog`
--

CREATE TABLE `design_catalog` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_bin NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `design_catalog`
--

INSERT INTO `design_catalog` (`id`, `name`, `description`, `created`) VALUES
(1, 'NỘI THẤT GIA ĐÌNH', 'NỘI THẤT GIA ĐÌNH', '2020-10-08 22:25:21'),
(2, 'NỘI THẤT VĂN PHÒNG', 'NỘI THẤT VĂN PHÒNG', '2020-10-08 22:28:27'),
(3, 'NỘI THẤT KHÁCH SẠN', 'NỘI THẤT KHÁCH SẠN', '2020-10-08 22:36:23'),
(4, 'NỘI THẤT CHUNG CƯ', 'NỘI THẤT CHUNG CƯ', '2020-10-08 22:36:48'),
(5, 'NỘI THẤT BIỆT THỰ', 'NỘI THẤT BIỆT THỰ', '2020-10-08 22:37:11');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` varchar(10) COLLATE utf8_bin NOT NULL,
  `percent` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `percent`, `date_start`, `date_end`, `created`) VALUES
('PF3RMD7L6T', 10, '2020-10-12', '2020-12-14', '2020-10-13 01:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `content` varchar(5000) COLLATE utf8mb4_bin DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `banner1` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `banner2` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `desc` varchar(10000) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `created`, `banner1`, `banner2`, `desc`) VALUES
(1, 'Ngôi nhà của những nếp gấp', '<p>Sống gần gũi với thi&ecirc;n nhi&ecirc;n l&agrave; một nhu cầu của con người, nhưng kh&ocirc;ng phải l&uacute;c n&agrave;o nhu cầu ấy cũng được đ&aacute;p ứng một c&aacute;ch triệt để, nhất l&agrave; với c&aacute;c ng&ocirc;i nh&agrave; phố ở đ&ocirc; thị hiện nay. Với ng&ocirc;i nh&agrave; n&agrave;y, nh&oacute;m thiết kế c&ograve;n phải xử l&yacute; cả yếu tố kh&iacute; hậu kh&aacute; đặc th&ugrave; của Huế &ndash; nơi được xem l&agrave; c&oacute; lượng mưa lớn nhất ở Việt Nam, thời gian mưa k&eacute;o d&agrave;i khiến rất nhiều c&ocirc;ng tr&igrave;nh phải t&igrave;m c&aacute;ch b&ecirc; t&ocirc;ng h&oacute;a nhằm chống lại sự khắc nghiệt của thời tiết. L&agrave;m sao để đưa gi&oacute;, &aacute;nh s&aacute;ng v&agrave;o trong ng&ocirc;i nh&agrave; nhưng mưa th&igrave; kh&ocirc;ng?</p>\r\n', '2020-10-12 17:06:27', 'noithat1.jpg', 'noithat2.jpg', '<p>Mang tr&ecirc;n m&igrave;nh phần ngoại h&igrave;nh đặc biệt, b&ecirc;n trong ng&ocirc;i nh&agrave; lại t&igrave;m đến sự kết hợp giữa c&aacute;c thiết bị hiện đại h&ograve;a v&agrave;o sự ấm c&uacute;ng của gỗ, gạch v&agrave; những mảng x&aacute;m xi măng tạo hiệu ứng rất tốt về kh&ocirc;ng gian. Sự kết hợp n&agrave;y l&agrave; ng&ocirc;n ngữ xuy&ecirc;n suốt của nh&oacute;m thiết kế nhằm phục vụ một mục đ&iacute;ch duy nhất, biến căn nh&agrave; th&agrave;nh một gia đ&igrave;nh đ&uacute;ng nghĩa l&agrave; tổ ấm. Về c&ocirc;ng năng, kh&ocirc;ng gian được chia l&agrave;m hai phần. Tầng trệt d&ugrave;ng cho việc lưu tr&uacute; của kh&aacute;ch, bạn b&egrave; của gia chủ. Với diện t&iacute;ch kh&ocirc;ng qu&aacute; lớn nhưng nh&oacute;m thiết kế đ&atilde; tạo ra được hai khu vực tắm v&agrave; vệ sinh lộ thi&ecirc;n.</p>\r\n'),
(4, 'Thiết kế ngoại thất nhà phố 3 tầng hiện đại', '<p>Tốc độ ph&aacute;t triển đ&ocirc; thị ng&agrave;y c&agrave;ng nhanh, diện t&iacute;ch đất x&acirc;y dựng ng&agrave;y c&agrave;ng thu hẹp, những căn nh&agrave; phố hiện đại đang trở th&agrave;nh xu hướng lựa chọn của nhiều gia chủ. Để tạo điều kiện thuận lợi cho những ai đang c&oacute; nhu cầu x&acirc;y dựng nh&agrave; ở m&agrave; chưa biết lựa chọn mẫu n&agrave;o ph&ugrave; hợp cho m&igrave;nh, HomeXinh sẽ giới thiệu tới bạn đọc mẫu&nbsp;thiết kế ngoại thất nh&agrave; phố 3 tầng hiện đại đẹp. C&ugrave;ng ngắm nh&igrave;n những h&igrave;nh ảnh tuyệt vời đ&oacute; trong b&agrave;i viết dưới đ&acirc;y.</p>\r\n', '2020-10-18 08:12:45', 'Screenshot_7.png', 'Screenshot_8.png', '<p>Chắc chắn gia chủ phải l&agrave; người hướng tới sự ho&agrave;n hảo. Bởi căn nh&agrave; hiện l&ecirc;n thật hiện đại, trẻ trung với kh&ocirc;ng gian xanh thư gi&atilde;n. M&agrave;u sắc của mẫu nh&agrave; phố được kết hợp rất tinh tế v&agrave; ph&ugrave; hợp. C&aacute;c gam m&agrave;u trung t&iacute;nh được sử dụng một c&aacute;ch tối đa nhất như m&agrave;u trắng, m&agrave;u n&acirc;u kết hợp c&ugrave;ng sắc xanh tươi m&aacute;t&hellip; Kh&ocirc;ng chỉ vậy, để tăng th&ecirc;m sự tương phản th&igrave; c&aacute;c kiến tr&uacute;c sư c&ograve;n sử dụng cả những gam m&agrave;u nổi bật như v&agrave;ng&hellip;</p>\r\n\r\n<p>Đặc điểm của phong c&aacute;ch thiết kế kiến tr&uacute;c hiện đại đ&oacute; l&agrave; kh&ocirc;ng cần phải tu&acirc;n theo những nguy&ecirc;n tắc cứng nhắc. Tại căn nh&agrave; phố hiện đại n&agrave;y, sự ph&oacute;ng kho&aacute;ng hiện đại đ&oacute; được thể hiện qua c&aacute;c h&igrave;nh khối thiết kế. Sự bất c&acirc;n xứng về c&aacute;c mảng khối kh&ocirc;ng gian lại đem tới sự h&agrave;i h&ograve;a đến kh&ocirc;ng ngờ. Thiết kế ngoại thất nh&agrave; phố 3 tầng hiện đại với cổng r&agrave;o đơn giản v&agrave; nổi bật đi k&egrave;m với những chi tiết v&agrave; m&agrave;u sắc sang trọng. Phần s&acirc;n trước rộng r&atilde;i được tận dụng th&agrave;nh nơi để xe.</p>\r\n\r\n<p>To&agrave;n bộ cửa sổ ở c&aacute;c tầng v&agrave; lan can ban c&ocirc;ng tầng 2 đều được sử dụng vật liệu bằng k&iacute;nh cường lực. Gi&uacute;p ng&ocirc;i nh&agrave; tận dụng được &aacute;nh s&aacute;ng mặt trời, tạo cảm gi&aacute;c th&ocirc;ng tho&aacute;ng v&agrave; rộng r&atilde;i cho kh&ocirc;ng gian nội thất b&ecirc;n trong căn nh&agrave;. Ngo&agrave;i ra c&ograve;n gi&uacute;p l&agrave;m nổi bật vẻ đẹp hiện đại v&agrave; v&ocirc; c&ugrave;ng sang trọng của mẫu thiết kế nh&agrave; n&agrave;y. Để đảm bảo an to&agrave;n th&igrave; khu vực ban c&ocirc;ng tầng trệt được bao k&iacute;n bằng k&iacute;nh, nhưng kh&ocirc;ng hề tạo cảm gi&aacute;c b&iacute; b&aacute;ch v&agrave; t&ugrave; t&uacute;ng. Lan can ban c&ocirc;ng ng&ocirc;i nh&agrave; được thiết kế đơn giản với chất liệu k&iacute;nh cường lực kết hợp với đ&oacute; l&agrave; c&aacute;c tiểu cảnh xanh tươi cũng gi&uacute;p t&ocirc; điểm n&eacute;t đẹp sang trọng cho ng&ocirc;i nh&agrave;, đồng thời duy tr&igrave; kh&ocirc;ng gian sống tho&aacute;ng m&aacute;t, trong l&agrave;nh.</p>\r\n'),
(5, 'Nội thất thông minh phù hợp với không gian chung cư', '<p>Đồ nội thất hiện đại, th&ocirc;ng minh hay c&ograve;n gọi l&agrave; đồ nội thất đa năng l&agrave; một d&ograve;ng d&ograve;ng sản phẩm được thiết kế t&iacute;ch hợp nhiều c&ocirc;ng năng sử dụng kh&aacute;c nhau. Thiết kế n&agrave;y nhằm mục đ&iacute;ch mang đến một kh&ocirc;ng gian sống tiện nghi v&agrave; hiện đại.</p>\r\n\r\n<p>B&ecirc;n cạnh đ&oacute;, ch&uacute;ng c&ograve;n gi&uacute;p tiết kiệm được diện t&iacute;ch kh&ocirc;ng gian m&agrave; vẫn đ&aacute;p ứng nhu cầu sinh hoạt của mọi th&agrave;nh vi&ecirc;nmột c&aacute;ch tốt nhất. So với những m&oacute;n đồ nội thất truyền thống th&igrave; c&aacute;c đồ nội thất đa năng c&oacute; nhiều ưu điểm hơn hẳn. Đ&oacute; ch&iacute;nh l&agrave; l&yacute; do m&agrave; ch&uacute;ng nhận được sự quan t&acirc;m v&agrave; ưa chuộng của c&aacute;c gia đ&igrave;nh Việt.</p>\r\n', '2020-10-18 08:15:54', 'Screenshot_9.png', 'Screenshot_10.png', '<p>Một trong những đồ nội thất đa năng kh&aacute;c ph&ugrave; hợp với ph&ograve;ng kh&aacute;ch của chung cư đ&oacute; l&agrave; sofa giường. Chỉ với thao t&aacute;c đơn giản l&agrave; bạn c&oacute; thể biến phong kh&aacute;ch th&agrave;nh nơi nghỉ ngơi v&agrave; ngược lại.</p>\r\n\r\n<p>Với cơ chế hoạt động n&acirc;ng l&ecirc;n hạ xuống nhẹ nh&agrave;ng ,đơn giản bạn c&oacute; thể sử dụng ch&uacute;ng một c&aacute;ch dễ d&agrave;ng. V&agrave;o ban ng&agrave;y, bạn n&acirc;ng giường l&ecirc;n đ&oacute; sẽ l&agrave; một bộ sofa tiếp kh&aacute;ch b&igrave;nh thường. V&agrave; khi bạn muốn nghỉ ngơi nhanh ch&oacute;ng, chỉ cần hạ xuống đơn giản. Hoặc khi gia đ&igrave;nh bạn c&oacute; kh&aacute;ch tới thăm nh&agrave;, chỉ cần k&eacute;p ch&acirc;n giường hai b&ecirc;n kệ l&agrave; bạn đ&atilde; c&oacute; nơi cho kh&aacute;ch nghỉ ngơi, thư gi&atilde;n.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `catalog_id` int(11) DEFAULT NULL,
  `com_id` int(11) DEFAULT NULL,
  `name` varchar(1000) COLLATE utf8_bin NOT NULL,
  `code` varchar(30) COLLATE utf8_bin NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `discount` int(11) DEFAULT 0 COMMENT '%',
  `img_link` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `img_list` text COLLATE utf8_bin DEFAULT NULL,
  `count_buy` int(11) DEFAULT 0,
  `count_view` int(11) DEFAULT 0,
  `detail` varchar(3000) COLLATE utf8_bin DEFAULT NULL,
  `created` datetime DEFAULT current_timestamp(),
  `item_count` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `catalog_id`, `com_id`, `name`, `code`, `price`, `discount`, `img_link`, `img_list`, `count_buy`, `count_view`, `detail`, `created`, `item_count`) VALUES
(1, 4, 1, 'Bộ bàn ăn phòng bếp', 'SP-289363', '1900000.00', 0, 'kitchen-1940174.jpg', NULL, 0, 12, '<p><strong>- Bộ sản phẩm bao gồm 01 b&agrave;n v&agrave; 06&nbsp;ghế</strong><br />\r\n<br />\r\n- M&agrave;u sắc: V&agrave;ng, N&acirc;u, Đen, Trắng&nbsp;<br />\r\n<br />\r\n- K&iacute;ch thước: d&agrave;i 75cm x rộng 75cm x cao 75cm<br />\r\n<br />\r\n- Chất Liệu: Gỗ cao su tự nhi&ecirc;n<br />\r\n<br />\r\n- Ghế ngồi bọc n&ecirc;m simili cao cấp.<br />\r\n<br />\r\n- Ghế lẻ:&nbsp;<strong>380.000đ/c&aacute;i</strong><br />\r\n<br />\r\n- B&agrave;n lẻ:&nbsp;<strong>1.500.000đ/c&aacute;i</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- H&agrave;ng xuất khẩu H&agrave;n Quốc mới 100% nguy&ecirc;n th&ugrave;ng nguy&ecirc;n kiện.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Đ&oacute;ng g&oacute;i trong th&ugrave;ng carton, dễ d&agrave;ng vận chuyển, kh&ocirc;ng lo tr&agrave;y xước.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- H&agrave;ng c&oacute; sẵn, giao trong ng&agrave;y, thanh to&aacute;n khi nhận h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Bảo h&agrave;nh: 2 năm tận nơi miễn ph&iacute;.</p>\r\n', '2020-10-13 21:38:04', 100),
(2, 9, 1, 'Bộ sofa phòng khách', 'SP-433469', '10000000.00', 10, 'imagesq.jpg', NULL, 0, 12, '<p>Nội thất quan trọng nhất của kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch ch&iacute;nh l&agrave; sofa nhưng kh&ocirc;ng phải ghế sofa n&agrave;o cũng ph&ugrave; hợp với kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch biệt thự. Sự lựa chọn hợp l&yacute; nhất l&agrave; sofa chữ L da c&ocirc;ng nghiệp m&agrave;u n&acirc;u nhạt trang nh&atilde;.</p>\r\n\r\n<p>Với loại sofa chữ L n&agrave;y, gia chủ c&oacute; thể vừa tiết kiệm tối đa kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch th&ocirc;ng qua việc tận dụng c&aacute;c g&oacute;c chết trong căn ph&ograve;ng. Chưa kể sofa đơn vu&ocirc;ng được sử dụng hợp l&yacute; c&ograve;n khiến cho ph&ograve;ng kh&aacute;ch c&oacute; điểm nhấn nhất định.</p>\r\n\r\n<p>M&agrave;u sắc của sofa da c&ocirc;ng nghiệp n&agrave;y cũng cực dễ phối với c&aacute;c m&agrave;u sắc kh&aacute;c, kể cả m&agrave;u v&acirc;n gỗ &oacute;c ch&oacute; hoặc m&agrave;u đen, trắng,&hellip; Tất cả đều tạo n&ecirc;n sự h&agrave;i h&ograve;a về bố cục m&agrave;u sắc trong kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch. Cụ thể, kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch biệt thự hiện đại được sử dụng b&agrave;n tr&agrave; k&iacute;nh Temper cường lực vu&ocirc;ng, bo 4 cạnh xung quanh, đen đặc trưng v&agrave; thiết kế c&aacute;ch điệu tạo cho ph&ograve;ng kh&aacute;ch điểm nhấn kh&aacute; h&agrave;i h&ograve;a với m&agrave;u sofa da c&ocirc;ng nghiệp n&acirc;u nhạt tr&ecirc;n.</p>\r\n', '2020-10-14 00:00:13', 100),
(3, 11, 1, 'Giường ngủ gỗ tự nhiên', 'SP-670643', '10000000.00', 0, 'bedroom-1940169.jpg', NULL, 0, 2, '<p>Bạn đang đi t&igrave;m kiếm mẫu giường&nbsp;ngủ&nbsp;cho gia đ&igrave;nh m&igrave;nh. H&atilde;y tham khảo mẫu sản phẩm n&agrave;y của Nội thất AMD Việt Nam ch&uacute;ng t&ocirc;i. Đ&acirc;y l&agrave; một trong những&nbsp;mẫu giường&nbsp;ngủ nhất&nbsp;v&agrave; được ưa chuộng nhất trong v&agrave;i năm qua, đặc biệt l&agrave; trong năm 2020 n&agrave;y. Khi được đưa ra sự lựa chọn c&aacute;c mẫu giường cho căn ph&ograve;ng ngủ nh&agrave; m&igrave;nh th&igrave; đa số kh&aacute;ch h&agrave;ng sẽ để &yacute; tới mẫu n&agrave;y v&agrave; lựa chọn. C&ograve;n qu&yacute; vị th&igrave; sao, bạn c&oacute; thấy mẫu n&agrave;y đẹp kh&ocirc;ng? h&atilde;y cho ch&uacute;ng t&ocirc;i biết về suy nghĩ của bạn nha.</p>\r\n', '2020-10-15 21:53:06', 100),
(4, 9, 1, 'Sofa góc gỗ tự nhiên bọc nỉ gi', 'SP-115085', '5900000.00', 5, 'living-room-2732939 (1).jpg', NULL, 0, 3, '<h2>Th&ocirc;ng tin Sofa g&oacute;c gỗ tự nhi&ecirc;n bọc nỉ gi&aacute; rẻ</h2>\r\n\r\n<p>Kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch l&agrave; nơi được rất nhiều gia đ&igrave;nh ch&uacute; &yacute;. Ch&iacute;nh v&igrave; thế m&agrave; tại đ&acirc;y họ thường trưng b&agrave;y những mẫu nội thất ph&ograve;ng kh&aacute;ch m&agrave; cụ thể l&agrave; những mẫu ghế&nbsp;<strong>sofa g&oacute;c gỗ tự nhi&ecirc;n bọc</strong><strong>&nbsp;nỉ gi&aacute; rẻ .</strong></p>\r\n\r\n<p>Với khung gỗ thịt chắc chắn c&ugrave;ng đệm d&agrave;y v&agrave; bọc ngo&agrave;i l&agrave; chất liệu nỉ với nhiều m&agrave;u kh&aacute;c nhau l&agrave;m những mẫu ghế n&agrave;y hiện đại v&agrave; chắc chắn hơn. Với lớp đệm thứ hai bạn c&oacute; thể th&aacute;o ra giặt được. N&oacute; gi&uacute;p bạn t&ocirc;n n&ecirc;n vẻ đẹp kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch nh&agrave; bạn.</p>\r\n\r\n<p>Đến với nội thất Thi&ecirc;n Ph&uacute;&nbsp;bạn sẽ chọn cho m&igrave;nh những mẫu ghế sofa nỉ th&ocirc;ng minh với nhiều m&agrave;u v&agrave; c&aacute;c k&iacute;ch thước kh&aacute;c nhau. Ngo&agrave;i ra, bạn c&ograve;n c&oacute; thể chọn cho m&igrave;nh những mẫu nội thất văn ph&ograve;ng v&agrave; gia đ&igrave;nh đẹp như:&nbsp;<strong>kệ trang tr&iacute; đẹp gi&aacute; rẻ</strong>,&nbsp;<strong>b&agrave;n họp thanh l&yacute; ch&acirc;n sắt bốn ch&acirc;n độc lập</strong>&nbsp;.</p>\r\n\r\n<h2><strong>Đặc đi&ecirc;̉m của&nbsp;sofa&nbsp;nỉ th&ocirc;ng minh gi&aacute; rẻ</strong></h2>\r\n\r\n<ul>\r\n	<li><strong>Chất liệu:</strong>&nbsp;Gỗ tự nhi&ecirc;n bọc nỉ</li>\r\n	<li><strong>K&iacute;ch thước</strong>: 2m5x1m6x75cm</li>\r\n	<li><strong>M&agrave;u sắc</strong>: T&ugrave;y chọn</li>\r\n	<li><strong>Bảo h&agrave;nh</strong>: 12 th&aacute;ng</li>\r\n	<li>Độ mới 100% chưa qua sử dụng.<br />\r\n	- G&iacute;a tr&ecirc;n chưa bao gồm:&nbsp;+ Đ&ocirc;n nhỏ 300,000/ 1 đ&ocirc;n)<br />\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; + Gối ( 150,000/ 1 chiếc)<br />\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; + ghế lẻ 1,590,000&nbsp;<br />\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; + B&agrave;n k&iacute;nh (t&ugrave;y theo mẫu kh&aacute;ch chọn ph&ugrave; hợp với sofa)</li>\r\n</ul>\r\n', '2020-10-18 11:34:34', 100),
(5, 12, 1, 'Combo Nội Thất Phòng Ngủ AMD', 'SP-764143', '32000000.00', 0, 'Screenshot_1.png', NULL, 0, 4, '<h2>Những ưu điểm của Combo Nội Thất Ph&ograve;ng Ngủ Deco CBP23</h2>\r\n\r\n<p>Combo Nội Thất Ph&ograve;ng Ngủ Deco CBP23 mang phong c&aacute;ch hiện đại ph&ugrave; hợp với nhiều kh&ocirc;ng gian sống gia đ&igrave;nh.</p>\r\n\r\n<p>C&oacute; nhiều m&agrave;u sắc v&agrave; k&iacute;ch thước đa dạng cho kh&aacute;ch h&agrave;ng lựa chọn</p>\r\n\r\n<p>Đ&aacute;p ứng mọi nhu cầu sử dụng nhờ chất lượng l&agrave; ưu ti&ecirc;n số 1</p>\r\n\r\n<p>Sản xuất tại xưởng tiết kiệm chi ph&iacute;, gi&aacute; th&agrave;nh phải chăng, ch&iacute;nh s&aacute;ch giao h&agrave;ng, lắp đặt tận nh&agrave;</p>\r\n\r\n<p>Bảo h&agrave;nh 24 th&aacute;ng</p>\r\n\r\n<h2>Lưu &yacute; về chất liệu</h2>\r\n\r\n<p>Gỗ MDF Minh Long đạt ti&ecirc;u chuẩn V313 c&oacute; khả năng chống ẩm tốt nhất hiện nay</p>\r\n\r\n<p>Được cung cấp bởi thương hiệu v&aacute;n gỗ c&ocirc;ng nghiệp h&agrave;ng đầu trong khu vực Panel Plus (Th&aacute;i Lan)</p>\r\n\r\n<p>Ti&ecirc;u chuẩn V313 (hoạt chất chống ẩm trong th&agrave;nh phần keo chiếm 21-24%) khả năng chống ẩm cao v&agrave; chịu lực tốt hơn so với gỗ th&ocirc;ng thường</p>\r\n\r\n<p>Phần l&otilde;i xanh l&agrave; chất chỉ thị m&agrave;u th&ecirc;m v&agrave;o để gi&uacute;p ph&acirc;n biệt gỗ chống ẩm với gỗ thường, kh&ocirc;ng l&agrave;m thay đổi th&ocirc;ng số kỹ thuật của gỗ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, nhiều kh&aacute;ch h&agrave;ng vẫn nghĩ rằng gỗ chống ẩm phải c&oacute; l&otilde;i m&agrave;u xanh.</p>\r\n\r\n<p>Lợi dụng quan niệm sai lầm về m&agrave;u sắc gỗ của kh&aacute;ch h&agrave;ng, những sản phẩm nội thất được l&agrave;m từ loại gỗ l&otilde;i xanh nhưng kh&ocirc;ng c&oacute; khả năng chống ẩm đ&atilde; c&oacute; cơ hội tr&ocirc;i nổi tr&ecirc;n thị trường.</p>\r\n\r\n<p>&ldquo;Đội lốt&rdquo; chống ẩm n&ecirc;n gi&aacute; th&agrave;nh cao hơn nhưng khả năng chống ẩm lại rất k&eacute;m dẫn đến tuổi thọ của sản phẩm thấp, dễ bị cong v&ecirc;nh v&agrave; hỏng h&oacute;c trong qu&aacute; tr&igrave;nh sử dụng.</p>\r\n\r\n<p>Kh&aacute;ch h&agrave;ng cần hết sức lưu &yacute; khi chọn sản phẩm để bảo vệ lợi &iacute;ch của ch&iacute;nh m&igrave;nh.</p>\r\n', '2020-10-18 11:39:32', 100),
(6, 9, 1, 'Sofa văng thiết kế hiện đại', 'SP-547667', '3700000.00', 0, 'living-room-2583032.jpg', NULL, 0, 4, '<h2><strong>Đặc đi&ecirc;̉m của&nbsp;ghế&nbsp;Sofa văng thiết kế hiện đại 1m8 gi&aacute; rẻ&nbsp;</strong></h2>\r\n\r\n<ul>\r\n	<li><strong>Chất liệu</strong>:&nbsp;Nỉ mềm mượt, &ecirc;m &aacute;i</li>\r\n	<li><strong>K&iacute;ch thước</strong>:&nbsp;D&agrave;i:1m8 x S&acirc;u:75cm</li>\r\n	<li><strong>M&agrave;u sắc</strong>:&nbsp; &nbsp;T&ugrave;y chọn theo y&ecirc;u cầu</li>\r\n	<li><strong>Đặt h&agrave;ng:&nbsp;</strong>Qu&yacute; kh&aacute;ch c&oacute; thể đặt h&agrave;ng theo m&agrave;u sắc, khung, chất liệu</li>\r\n</ul>\r\n', '2020-10-18 11:44:02', 100),
(7, 9, 1, 'Sofa da văng gỗ xám', 'SP-718881', '2000000.00', 5, 'noithat2.jpg', NULL, 0, 1, '<h2><strong>Sofa da g&oacute;c đen sọc trắng tại H&agrave; Nội</strong></h2>\r\n\r\n<p>Kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch l&agrave; nơi được rất nhiều gia đ&igrave;nh ch&uacute; &yacute;. Ch&iacute;nh v&igrave; thế m&agrave; tại đ&acirc;y họ thường trưng b&agrave;y những mẫu nội thất ph&ograve;ng kh&aacute;ch m&agrave; cụ thể l&agrave; những mẫu ghế sofa da g&oacute;c đen sọc trắng .</p>\r\n\r\n<p>Với khung gỗ thịt chắc chắn c&ugrave;ng đệm d&agrave;y v&agrave; bọc ngo&agrave;i l&agrave; chất liệu da với nhiều m&agrave;u kh&aacute;c nhau l&agrave;m những mẫu ghế n&agrave;y hiện đại v&agrave; chắc chắn hơn. N&oacute; gi&uacute;p bạn t&ocirc;n n&ecirc;n vẻ đẹp kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch nh&agrave; bạn.</p>\r\n\r\n<p>Đến với nội thất Thi&ecirc;n ph&uacute;&nbsp;bạn sẽ chọn cho m&igrave;nh những mẫu ghế sofa da cao cấp với nhiều m&agrave;u v&agrave; c&aacute;c k&iacute;ch thước kh&aacute;c nhau. Ngo&agrave;i ra, bạn c&ograve;n c&oacute; thể chọn cho m&igrave;nh những mẫu nội thất văn ph&ograve;ng v&agrave; gia đ&igrave;nh đẹp như:&nbsp;<strong>ghế ch&acirc;n quỳ lưng da thấp gi&aacute; rẻ, b&agrave;n họp h&igrave;nh chữ nhật gi&aacute; rẻ 2m4,&hellip;.</strong></p>\r\n\r\n<h2><strong>Đặc đi&ecirc;̉m của sofa da g&oacute;c đen sọc trắng :</strong></h2>\r\n\r\n<ul>\r\n	<li>Chất liệu&nbsp;sofa da g&oacute;c đen sọc trắng l&agrave;&nbsp;Khung gỗ, da</li>\r\n	<li>K&iacute;ch thước:&nbsp;&nbsp;2100 x 1600mm&nbsp;C&oacute; thể xoay chiều theo &yacute; muốn</li>\r\n	<li>M&agrave;u sắc:&nbsp; &nbsp;caro đen trắng</li>\r\n	<li>Độ mới 100% chưa qua sử dụng.<br />\r\n	- G&iacute;a tr&ecirc;n chưa bao gồm:&nbsp; +Gối ( 150,000/ 1 chiếc)<br />\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; + B&agrave;n k&iacute;nh (t&ugrave;y theo mẫu kh&aacute;ch chọn ph&ugrave; hợp với sofa)</li>\r\n</ul>\r\n', '2020-10-18 11:50:38', 100),
(8, 11, 1, 'Giường ngủ gỗ kiểu Nhật', 'SP-064092', '17000000.00', 0, 'giuongnhat.png', NULL, 0, 12, '<p>Mẫu giường ngủ phong c&aacute;ch Nhật Bản Deco&nbsp;được kỳ vọng sẽ l&agrave;m h&agrave;i l&ograve;ng những vị kh&aacute;ch h&agrave;ng y&ecirc;u th&iacute;ch phong c&aacute;ch nội thất tối giản của đất nước mặt trời mọc. Sản phẩm được thiết kế theo lối tối giản, d&aacute;ng thấp mang đến người d&ugrave;ng cảm gi&aacute;c thoải m&aacute;i khi sử dụng.</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 giường ngủ phong c&aacute;ch Nhật Bản, 2 tab 2 b&ecirc;n như h&igrave;nh ( chưa t&iacute;nh đệm nằm)</p>\r\n\r\n<p>K&iacute;ch thước : Thường l&agrave;m k&iacute;ch thước ph&ugrave; hợp với k&iacute;ch thước đệm 1m6x2m hoặc 1m8x2m hoặc bạn cũng c&oacute; thể đặt k&iacute;ch thước theo nhu cầu của m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : gỗ tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc m&agrave;u theo y&ecirc;u cầu của bạn để ph&ugrave; hợp với m&agrave;u nội thất hiện c&oacute; trong ph&ograve;ng. Bạn vui l&ograve;ng li&ecirc;n hệ với Deco để trao đổi trực tiếp.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-18 11:56:25', 100),
(9, 3, 2, 'Bàn làm việc gỗ ép trắng', 'SP-301560', '3000000.00', 0, 'banlamviec.png', NULL, 0, 1, '<p>B&agrave;n&nbsp;thiết kế v&ocirc; c&ugrave;ng nhỏ gọn nhưng lại cung cấp cho người d&ugrave;ng một kh&ocirc;ng gian l&agrave;m việc, lưu trữ t&agrave;i liệu đầy đủ v&agrave; tiện nghi đến bất ngờ.</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm b&agrave;n l&agrave;m việc nhỏ gọn, tiện dụng như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (D x R x C): 1330 x 535 x 750mm. Bạn cũng c&oacute; thể thay đổi k&iacute;ch thước theo nhu cầu v&agrave; sở th&iacute;ch.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phủ Melamine m&agrave;u hoặc v&acirc;n gỗ, ch&acirc;n gỗ tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Bộ sản phẩm n&agrave;y phong c&aacute;ch hiện đại, sang trọng, v&agrave; được l&agrave;m từ gỗ MDF cao cấp n&ecirc;n c&oacute; độ bền kh&aacute; cao, chắc chắn, kh&ocirc;ng cong v&ecirc;nh, mối mọt khi sử dụng.</p>\r\n\r\n<p>Sản phẩm mang phong c&aacute;ch hiện đại, kiểu d&aacute;ng nhỏ gọn, ph&ugrave; hợp với nhiều kh&ocirc;ng gian sống gia đ&igrave;nh.</p>\r\n\r\n<p>B&agrave;n l&agrave;m việc tại nh&agrave; c&oacute; nhiều m&agrave;u sắc v&agrave; k&iacute;ch thước đa dạng cho kh&aacute;ch h&agrave;ng lựa chọn</p>\r\n\r\n<p>Chất lượng sản phẩm vượt trội, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n', '2020-10-18 12:02:06', 80),
(10, 11, 1, 'Giường ngủ hiện đại gỗ trắng', 'SP-582885', '17000000.00', 0, 'giuonghd1.png', NULL, 0, 1, '<p>Sẽ rất tiếc nuối nếu bạn bỏ lỡ mẫu giường ngủ gia đ&igrave;nh phong c&aacute;ch hiện đại AMD khi đến AMD mua sắm đồ nội thất cho gia đ&igrave;nh.</p>\r\n\r\n<p>Giường ngủ t&iacute;ch hợp c&aacute;c ngăn chưa đồ gi&uacute;p tiết kiệm kh&ocirc;ng gian, tận dụng khoảng trống ph&iacute;a dưới giường. Bạn sẽ đỡ phải mua th&ecirc;m tủ quần &aacute;o để chứa đồ, từ đ&oacute; gi&uacute;p tiết kiệm chi ph&iacute; rất đ&aacute;ng kể.</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 giường ngủ hiện đại như h&igrave;nh, gồm 2 tab 2 b&ecirc;n, đệm tựa đầu giường (chưa t&iacute;nh đệm nằm)</p>\r\n\r\n<p>K&iacute;ch thước : Thường l&agrave;m k&iacute;ch thước ph&ugrave; hợp với k&iacute;ch thước đệm 1m6x2m hoặc 1m8x2m hoặc bạn cũng c&oacute; thể đặt k&iacute;ch thước theo nhu cầu của m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long phủ melamin l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước tốt kh&ocirc;ng lo trời nồm.</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc m&agrave;u theo y&ecirc;u cầu của bạn để ph&ugrave; hợp với m&agrave;u nội thất hiện c&oacute; trong ph&ograve;ng. Bạn vui l&ograve;ng li&ecirc;n hệ với Deco để trao đổi trực tiếp.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Những ưu điểm sản phẩm</h2>\r\n\r\n<p>Giường&nbsp; mang phong c&aacute;ch hiện đại ph&ugrave; hợp với nhiều kh&ocirc;ng gian sống gia đ&igrave;nh.</p>\r\n\r\n<p>C&oacute; nhiều m&agrave;u sắc v&agrave; k&iacute;ch thước đa dạng cho kh&aacute;ch h&agrave;ng lựa chọn</p>\r\n\r\n<p>Chất lượng sản phẩm vượt trội, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n\r\n<p>Sản xuất tại xưởng tiết kiệm chi ph&iacute;, gi&aacute; th&agrave;nh phải chăng, ch&iacute;nh s&aacute;ch giao h&agrave;ng, lắp đặt tận nh&agrave;</p>\r\n\r\n<p>Bảo h&agrave;nh 24 th&aacute;ng</p>\r\n\r\n<h2>Lưu &yacute; về chất liệu</h2>\r\n\r\n<p>Gỗ MDF Minh Long đạt ti&ecirc;u chuẩn V313 c&oacute; khả năng chống ẩm tốt nhất hiện nay</p>\r\n\r\n<p>Được cung cấp bởi thương hiệu v&aacute;n gỗ c&ocirc;ng nghiệp h&agrave;ng đầu trong khu vực Panel Plus (Th&aacute;i Lan)</p>\r\n\r\n<p>Ti&ecirc;u chuẩn V313 (hoạt chất chống ẩm trong th&agrave;nh phần keo chiếm 21-24%) khả năng chống ẩm cao v&agrave; chịu lực tốt hơn so với gỗ th&ocirc;ng thường</p>\r\n\r\n<p>Phần l&otilde;i xanh l&agrave; chất chỉ thị m&agrave;u th&ecirc;m v&agrave;o để gi&uacute;p ph&acirc;n biệt gỗ chống ẩm với gỗ thường, kh&ocirc;ng l&agrave;m thay đổi th&ocirc;ng số kỹ thuật của gỗ.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, nhiều kh&aacute;ch h&agrave;ng vẫn nghĩ rằng gỗ chống ẩm phải c&oacute; l&otilde;i m&agrave;u xanh.</p>\r\n', '2020-10-18 12:08:25', 100),
(11, 13, 1, 'Tủ áo gỗ trắng sữa cho gia đình', 'SP-036938', '6700000.00', 0, 'tu_ao1.png', NULL, 0, 5, '<p>Tủ quần &aacute;o đẹp DTA39 c&oacute; kiểu d&aacute;ng kh&aacute; nhỏ gọn với nhiều ngăn lưu trữ đa dạng mang đến người d&ugrave;ng nhiều tiện &iacute;ch khi sử dụng.</p>\r\n\r\n<p>M&atilde; sản phẩm : DTA39</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm tủ &aacute;o gồm nhiều ngăn chứa đồ như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (DxRxC): 1200 x 580 x 2000mm Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phủ Melamine m&agrave;u v&agrave; v&acirc;n gỗ, khung tủ gỗ tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Ưu điểm kh&ocirc;ng thể kh&ocirc;ng kể t&ecirc;n của tủ quần &aacute;o gỗ hiện đại DTA39</h2>\r\n\r\n<p>Kiểu d&aacute;ng nhỏ gọn, tiện lợi</p>\r\n\r\n<p>Mẫu m&atilde; sản phẩm đa dạng, phong ph&uacute;</p>\r\n\r\n<p>Chất lượng cao cấp, tuổi thọ cao</p>\r\n\r\n<p>Gi&aacute; th&agrave;nh phải chăng, dịch vụ tận t&acirc;m</p>\r\n', '2020-10-18 13:42:08', 100),
(12, 13, 1, 'Tủ áo phòng ngủ gỗ bốn ngăn', 'SP-035418', '6000000.00', 0, 'tu_ao2.png', NULL, 0, 2, '<p>Tủ quần &aacute;o đẹp DTA33 được l&agrave;m từ chất liệu gỗ c&ocirc;ng nghiệp l&otilde;i xanh cao cấp chống được mối mọt, ẩm mốc, ph&ugrave; hợp với điều kiện thời tiết tại Việt Nam, mang phong c&aacute;ch hiện đại với mức gi&aacute; hợp l&yacute;</p>\r\n\r\n<p>M&atilde; sản phẩm : DTA33</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm tủ &aacute;o gia đ&igrave;nh với thiết kế v&ocirc; c&ugrave;ng tiện dụng như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (DxRxC): 800 x 550 x 2000mm Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phủ Melamine m&agrave;u kết hợp v&acirc;n gỗ tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>4 ưu điểm kh&ocirc;ng thể kh&ocirc;ng kể t&ecirc;n của tủ quần &aacute;o gỗ hiện đại DTA33</h2>\r\n\r\n<p>Thiết kế đầy tiện lợi cho người d&ugrave;ng</p>\r\n\r\n<p>Chất lượng cao, nguy&ecirc;n liệu tốt</p>\r\n\r\n<p>Mẫu m&atilde; đa dạng</p>\r\n\r\n<p>Gi&aacute; th&agrave;nh phải chăng, dịch vụ tận t&acirc;m</p>\r\n', '2020-10-18 13:51:54', 100),
(13, 13, 1, 'Tủ quần áo gỗ cửa trắng DTA44', 'SP-810042', '7000000.00', 0, 'tu_ao3.png', NULL, 0, 2, '<p>Tủ quần &aacute;o đẹp DTA44 c&oacute; kiểu d&aacute;ng nhỏ, gọn, đẹp, được l&ograve;ng của rất nhiều kh&aacute;ch h&agrave;ng</p>\r\n\r\n<p>M&atilde; sản phẩm : DTA44</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm tủ &aacute;o gia đ&igrave;nh như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (D x R x C): 1250 x 600 x 1850mm&nbsp;Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phủ Melamine v&acirc;n gỗ, m&agrave;u, viền v&agrave; ch&acirc;n gỗ tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Ưu điểm kh&ocirc;ng thể kh&ocirc;ng kể t&ecirc;n của tủ quần &aacute;o gỗ hiện đại DTA44</h2>\r\n\r\n<p>Sở hữu thiết kế tiện dụng với người d&ugrave;ng, phong c&aacute;ch hiện đại, đi đầu xu hướng</p>\r\n\r\n<p>Kiểu d&aacute;ng, mẫu m&atilde; sản phẩm phong ph&uacute;, đa dạng</p>\r\n\r\n<p>Chất lượng sản phẩm vượt trội, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n\r\n<p>Gi&aacute; th&agrave;nh phải chăng, dịch vụ tận t&acirc;m</p>\r\n', '2020-10-18 14:06:22', 100),
(14, 3, 1, 'Bàn làm việc hiện đại có kệ sách', 'SP-488327', '5000000.00', 0, 'banlamv5.png', NULL, 0, 0, '<p>B&agrave;n L&agrave;m Việc Đẹp Deco DLV128 k&iacute;ch thước lớn, đa năng, sẵn s&agrave;ng đ&aacute;p ứng tất cả nhu cầu về lưu trữ t&agrave;i liệu cũng như l&agrave;m việc của bạn.</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm b&agrave;n như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước(D x R x C): 1200 x 600 x 1600mm. Bạn cũng c&oacute; thể thay đổi k&iacute;ch thước theo nhu cầu v&agrave; sở th&iacute;ch.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phun sơn chất lượng cao</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Bộ sản phẩm n&agrave;y phong c&aacute;ch hiện đại, sang trọng, v&agrave; được l&agrave;m từ gỗ MDF cao cấp n&ecirc;n c&oacute; độ bền kh&aacute; cao, chắc chắn, kh&ocirc;ng cong v&ecirc;nh, mối mọt khi sử dụng.</p>\r\n\r\n<p>Thiết kế k&iacute;ch thước lớn, nhiều ngăn đa năng, đ&aacute;p ứng nhu cầu tại nhiều kh&ocirc;ng gian</p>\r\n\r\n<p>Dễ d&agrave;ng b&agrave;i tr&iacute; trong kh&ocirc;ng gian ph&ograve;ng l&agrave;m việc c&aacute; nh&acirc;n hoặc gia đ&igrave;nh</p>\r\n\r\n<p>Chất liệu gỗ MDF đảm bảo độ bền, chống cong v&ecirc;nh, mối mọt</p>\r\n\r\n<p>Bạn được chọn m&agrave;u phun sơn tại cửa h&agrave;ng, đảm bảo đ&aacute;p ứng mọi sở th&iacute;ch trang tr&iacute;</p>\r\n\r\n<p>B&agrave;n l&agrave;m việc l&agrave; một sản phẩm đồ d&ugrave;ng nội thất quan trọng cho mỗi doanh nghiệp, mỗi c&ocirc;ng ty hay mỗi gia đ&igrave;nh. Từ nh&acirc;n vi&ecirc;n đến trưởng ph&ograve;ng cấp l&atilde;nh đạo hay c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh ai ai cũng phải sở hữu cho m&igrave;nh một chiếc. B&agrave;n văn ph&ograve;ng ch&iacute;nh l&agrave; điểm tựa để mỗi c&aacute; nh&acirc;n thực hiện việc đ&oacute;ng g&oacute;p cho tập thể doanh nghiệp c&ocirc;ng ty, gia đ&igrave;nh v&agrave; ch&iacute;nh bản th&acirc;n m&igrave;nh.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-18 14:11:56', 100),
(15, 11, 1, 'Giường ngủ kiểu Nhật gỗ sang trọng', 'SP-070446', '20000000.00', 0, 'giuongngus.png', NULL, 0, 1, '<p>Mẫu giường ngủ phong c&aacute;ch Nhật Bản Deco DGN08 ch&iacute;nh l&agrave; một lựa chọn kh&ocirc;ng thể bỏ lỡ với những ai y&ecirc;u th&iacute;ch sự mộc mạc, giản dị, gần gũi. Kh&ocirc;ng qu&aacute; đặc biệt đường n&eacute;t thiết kế, mẫu giường ngủ g&acirc;y ấn tượng với chiều cao thấp, thiết kế đơn giản v&agrave; cảm gi&aacute;c ấm c&uacute;ng m&agrave; ch&iacute;nh n&oacute; mang đến.</p>\r\n\r\n<p>M&atilde; sản phẩm :DGN08</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 giường ngủ phong c&aacute;ch Nhật Bản, 2 tab 2 b&ecirc;n như h&igrave;nh (chưa t&iacute;nh đệm nằm)</p>\r\n\r\n<p>K&iacute;ch thước : Thường l&agrave;m k&iacute;ch thước ph&ugrave; hợp với k&iacute;ch thước đệm 1m6x2m hoặc 1m8x2m hoặc bạn cũng c&oacute; thể đặt k&iacute;ch thước theo nhu cầu của m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : Gỗ sồi 100%</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc m&agrave;u theo y&ecirc;u cầu của bạn để ph&ugrave; hợp với m&agrave;u nội thất hiện c&oacute; trong ph&ograve;ng. Bạn vui l&ograve;ng li&ecirc;n hệ với Deco để trao đổi trực tiếp.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Những ưu điểm của Giường Ngủ Kiểu Nhật Deco DGN08</h2>\r\n\r\n<p>Giường ngủ đẹp Deco DGN08 thiết kế tinh tế, thanh lịch, tối giản v&agrave; trau chuốt từng đường n&eacute;t theo phong c&aacute;ch Nhật Bản</p>\r\n\r\n<p>C&oacute; nhiều m&agrave;u sắc v&agrave; k&iacute;ch thước đa dạng cho kh&aacute;ch h&agrave;ng lựa chọn</p>\r\n\r\n<p>Chất lượng sản phẩm vượt trội, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n\r\n<p>Sản xuất tại xưởng tiết kiệm chi ph&iacute;, gi&aacute; th&agrave;nh phải chăng, ch&iacute;nh s&aacute;ch giao h&agrave;ng, lắp đặt tận nh&agrave;</p>\r\n\r\n<p>Bảo h&agrave;nh 24 th&aacute;ng</p>\r\n', '2020-10-18 14:17:52', 100),
(16, 13, 1, 'Tủ áo phòng ngủ lớn bốn ngăn', 'SP-818109', '13000000.00', 0, 'tuaopnlon.png', NULL, 0, 1, '<p>Tủ quần &aacute;o đẹp DTA14 khiến bạn phải xu&yacute;t xoa mỗi khi ngắm nh&igrave;n. Sản phẩm vừa c&oacute; thiết kế th&ocirc;ng minh, tiện dụng lại c&oacute; được chất lượng ho&agrave;n hảo v&agrave; tuổi thọ l&acirc;u d&agrave;i.</p>\r\n\r\n<p>M&atilde; sản phẩm : DTA14</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 01 tủ 3 c&aacute;nh v&agrave; tủ cửa k&iacute;nh nhiều ngăn lưu trữ như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (D x R x C): k&iacute;ch thước sản phẩm</p>\r\n\r\n<p>Tủ 3 c&aacute;nh: 1800 x 600 x 2000mm</p>\r\n\r\n<p>Tủ cửa k&iacute;nh: 400 x 600 x 2000mm</p>\r\n\r\n<p>Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phun sơn chất lượng cao</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Ưu điểm nổi bật của tủ gỗ đựng quần &aacute;o thiết kế đẹp DTA14</h2>\r\n\r\n<p>Thiết kế sản phẩm mang phong c&aacute;ch hiện đại, kiểu d&aacute;ng nhỏ gọn, ph&ugrave; hợp với nhiều kh&ocirc;ng gian sống gia đ&igrave;nh.</p>\r\n\r\n<p>Chất lượng sản phẩm ho&agrave;n hảo, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n\r\n<p>Gi&aacute; th&agrave;nh phải chăng, ch&iacute;nh s&aacute;ch giao h&agrave;ng, lắp đặt tận nh&agrave;, bảo h&agrave;nh 24 th&aacute;ng</p>\r\n', '2020-10-18 14:20:19', 100),
(17, 3, 1, 'Bàn làm việc gỗ nhỏ gọn hiện đại', 'SP-154767', '3000000.00', 0, 'ban2.png', NULL, 0, 0, '<h1>B&agrave;n L&agrave;m Việc Deco DLV02</h1>\r\n\r\n<p>M&atilde; sản phẩm : DLV02</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 b&agrave;n l&agrave;m việc như h&igrave;nh.</p>\r\n\r\n<p>K&iacute;ch thước : Cao 72cm d&agrave;i 100cm rộng 45cm. C&oacute; thể thay đổi k&iacute;ch thước theo nhu cầu v&agrave; sở th&iacute;ch.</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long phủ melamin l&otilde;i xanh chống nước chống xước</p>\r\n\r\n<p>M&agrave;u sắc: m&agrave;u v&acirc;n gỗ như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Bộ b&agrave;n n&agrave;y kh&aacute; l&agrave; &ldquo;truyền thống&rdquo;, t&iacute;nh thực dụng cao, đơn giản nhưng hiệu quả, c&oacute; thể d&ugrave;ng để m&aacute;y t&iacute;nh case, laptop, t&agrave;i liệu để l&agrave;m việc.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-18 14:33:23', 100),
(18, 6, 1, 'Combo bộ tủ bếp lớn gỗ sơn trắng', 'SP-066097', '10000000.00', 0, 'Screeb.png', NULL, 0, 0, '<p>Với mỗi gia đ&igrave;nh Tủ bếp thường được lắp ngay sau khi x&acirc;y nh&agrave; xong v&agrave; sử dụng trong khoảng thời gian rất d&agrave;i (trung b&igrave;nh tr&ecirc;n 10 năm). Mặt kh&aacute;c hệ thống Tủ bếp c&ograve;n được li&ecirc;n kết với c&aacute;c đường ống điện nước phức tạp. Sẽ rất kh&oacute; khăn nếu c&oacute; hỏng h&oacute;c, phiền phức, tốn thời gian v&agrave; chi ph&iacute; sửa chữa cũng kh&ocirc;ng hề nhỏ v&igrave; gia chủ kh&ocirc;ng thể tự sửa được m&agrave; phải thu&ecirc; thợ c&oacute; chuy&ecirc;n m&ocirc;n. Ch&iacute;nh v&igrave; vậy việc đầu tư một lần cho sản phẩm chất lượng tốt sẽ gi&uacute;p bạn tr&aacute;nh được rủi ro sau n&agrave;y v&agrave; tiết kiệm chi ph&iacute; cho tương lai. Hay chi tiền một c&aacute;ch th&ocirc;ng minh!</p>\r\n\r\n<p>Xưởng nội thất Deco Miễn ph&iacute; thiết kế 2D, 3D gi&uacute;p gia chủ c&oacute; thể h&igrave;nh dung r&otilde; hơn v&agrave; cử đội ngũ kiến tr&uacute;c sư gi&agrave;u năm kinh nghiệm đến tận nơi đo đạc kĩ lưỡng</p>\r\n\r\n<p>M&atilde; sản phẩm: DTB14</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm Tủ bếp tr&ecirc;n, Tủ bếp dưới như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước</p>\r\n\r\n<p>Tủ bếp tr&ecirc;n d&agrave;i 3m cao 1m</p>\r\n\r\n<p>Tủ bếp dưới d&agrave;i 3m5 cao 0,8m</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long phủ melamin l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc m&agrave;u theo y&ecirc;u cầu của bạn để ph&ugrave; hợp với m&agrave;u nội thất hiện c&oacute; trong ph&ograve;ng.</p>\r\n', '2020-10-18 14:35:49', 100),
(19, 3, 1, 'Bàn làm việc gỗ giá rẻ', 'SP-575774', '3400000.00', 0, 'Screenshot_2.png', NULL, 0, 0, '<h1>B&agrave;n L&agrave;m Việc Deco DLV02</h1>\r\n\r\n<p>M&atilde; sản phẩm : DLV02</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 b&agrave;n l&agrave;m việc như h&igrave;nh.</p>\r\n\r\n<p>K&iacute;ch thước : Cao 72cm d&agrave;i 100cm rộng 45cm. C&oacute; thể thay đổi k&iacute;ch thước theo nhu cầu v&agrave; sở th&iacute;ch.</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long phủ melamin l&otilde;i xanh chống nước chống xước</p>\r\n\r\n<p>M&agrave;u sắc: m&agrave;u v&acirc;n gỗ như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Bộ b&agrave;n n&agrave;y kh&aacute; l&agrave; &ldquo;truyền thống&rdquo;, t&iacute;nh thực dụng cao, đơn giản nhưng hiệu quả, c&oacute; thể d&ugrave;ng để m&aacute;y t&iacute;nh case, laptop, t&agrave;i liệu để l&agrave;m việc.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-18 14:40:50', 100),
(20, 13, 1, 'Tủ áo phòng ngủ sang trọng hiện đại', 'SP-473715', '20000000.00', 0, 'tuao9.png', NULL, 0, 3, '<h1>Tủ &aacute;o ph&ograve;ng ngủ sang trọng hiện đại</h1>\r\n\r\n<p>Tủ quần &aacute;o đẹp DTA36 c&oacute; phong c&aacute;ch thiết kế thanh lịch, ph&ugrave; hợp với nhiều phong c&aacute;ch nội thất gia đ&igrave;nh kh&aacute;c nhau, mang đến người d&ugrave;ng kh&ocirc;ng gian lưu trữ đầy tiện lợi.</p>\r\n\r\n<p>M&atilde; sản phẩm : DTA36</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm tủ &aacute;o gồm nhiều ngăn chứa đồ như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước (DxRxC): 2000 x 600 x 2300mm Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu: Gỗ MDF Minh Long l&otilde;i xanh chống ẩm phủ Melamine m&agrave;u v&agrave; v&acirc;n gỗ, tay nắm cửa bằng gỗ sồi tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: như h&igrave;nh hoặc bạn cũng c&oacute; thể đặt m&agrave;u theo nhu cầu của m&igrave;nh, mọi chi tiết xin vui l&ograve;ng li&ecirc;n hệ với ch&uacute;ng t&ocirc;i để x&aacute;c nhận cụ thể.</p>\r\n\r\n<p>Bảo h&agrave;nh: 24 th&aacute;ng</p>\r\n\r\n<p>Phụ kiện: Ray bi bản lề ch&iacute;nh h&atilde;ng Caryny</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-18 14:53:17', 80),
(21, 11, 1, 'Giường gỗ thông 1m6 trắng kem', 'SP-621642', '4200000.00', 0, 'giuonggothong.png', NULL, 0, 2, '<p>Được thiết kế theo phong c&aacute;ch Ch&acirc;u &Acirc;u đương đại, giường ngủ MP04 g&oacute;p phần l&agrave;m ho&agrave;n hảo cho kh&ocirc;ng gian ph&ograve;ng ngủ của bạn.</p>\r\n\r\n<p>Giường được l&agrave;m từ gỗ th&ocirc;ng nhập Newzealand, với sự kết hợp giữa 2 gam m&agrave;u: trắng kem v&agrave; n&acirc;u tạo n&eacute;t sang trọng. Bạn c&oacute; thể chọn mua giường với k&iacute;ch thước King Size hoặc Queen Size trong bộ sưu tập&nbsp;<a href=\"http://minhandmore.com/search?q=gi%C6%B0%E1%BB%9Dng+mp04&amp;type=product\">giường MP04</a>&nbsp;đẹp mắt v&agrave; tinh tế.</p>\r\n\r\n<p><strong>K&iacute;ch thước lọt nệm:</strong></p>\r\n\r\n<p>Rộng 160 x D&agrave;i 200.</p>\r\n\r\n<p><strong>Chất liệu:</strong>&nbsp;</p>\r\n\r\n<p>Gỗ Th&ocirc;ng nhập khẩu.</p>\r\n\r\n<p><strong>M&agrave;u sắc:</strong></p>\r\n\r\n<p>Th&acirc;n sơn m&agrave;u kem, qu&eacute;t cọ tạo v&acirc;n, n&oacute;c sơn n&acirc;u, qu&eacute;t cọ tạo v&acirc;n.</p>\r\n', '2020-10-24 10:15:14', 100),
(22, 14, 1, 'Kệ Tivi M&M Midnight MV11M5', 'SP-640184', '2800000.00', 0, 'ketv.png', NULL, 0, 0, '<p><strong>K&iacute;ch thước:</strong></p>\r\n\r\n<p>Ngang 150 x S&acirc;u 40 x Cao 55 cm.</p>\r\n\r\n<p><strong>Chất liệu:</strong></p>\r\n\r\n<p>Tr&agrave;m + MDF</p>\r\n\r\n<p><strong>M&agrave;u sắc:</strong></p>\r\n\r\n<p>X&aacute;m ghi.</p>\r\n', '2020-10-24 10:21:45', 100),
(23, 14, 1, 'Kệ TV để sàn trắng kem sữa DB03', 'SP-195329', '5000000.00', 0, 'ketv44.png', NULL, 0, 7, '<h1>Kệ tivi để s&agrave;n&nbsp;Deco DB03</h1>\r\n\r\n<p>M&atilde; sản phẩm : DB03</p>\r\n\r\n<p>Quy c&aacute;ch: bộ gồm 1 kệ tivi để s&agrave;n như h&igrave;nh.</p>\r\n\r\n<p>K&iacute;ch thước:Bộ kệ tivi để s&agrave;n n&agrave;y cao 40cm rộng 35cm chiều d&agrave;i c&oacute; thể thay đổi từ 210cm đến 340cm.</p>\r\n\r\n<p>Chất liệu : Kệ l&agrave;m từ gỗ MDF Minh Long phủ melamin rồi sơn phủ bằng sơn c&ocirc;ng nghiệp cao cấp, m&agrave;u sắc tươi s&aacute;ng, bền m&agrave;u.</p>\r\n\r\n<p>M&agrave;u sắc: Trắng như h&igrave;nh hoặc trắng kết hợp đen hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Bộ kệ tivi n&agrave;y ph&ugrave; hợp với căn hộ chung cư hay những nh&agrave; c&oacute; thiết kế kiểu hiện đại, kết hợp tốt với sofa v&agrave; b&agrave;n tr&agrave;</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:27:38', 100),
(24, 14, 1, 'Kệ Tivi treo tường gỗ Deco TV82 ', 'SP-643082', '3080000.00', 0, 'ketvtreotuong.png', NULL, 0, 0, '<p>Kệ Tivi treo tường&nbsp;Deco TV82</p>\r\n\r\n<p>M&atilde; sản phẩm : TV82</p>\r\n\r\n<p>Quy c&aacute;ch: bộ gồm kệ 2 ngăn k&eacute;o dưới tivi v&agrave; c&aacute;c khung kệ b&ecirc;n tr&ecirc;n v&agrave; xung quanh tivi như h&igrave;nh, phần kệ n&agrave;y s&acirc;u 15cm.( kh&ocirc;ng bao gồm đồ tr&ecirc;n kệ v&agrave; c&aacute;c chi tiết trang tr&iacute; d&aacute;n tr&ecirc;n tường)</p>\r\n\r\n<p>K&iacute;ch thước: Kệ 2 ngăn k&eacute;o dưới tivi d&agrave;i 150cm cao 20cm s&acirc;u 30cm , c&aacute;c kệ xung quanh tivi s&acirc;u 13cm</p>\r\n\r\n<p>Chất liệu : Kệ l&agrave;m từ gỗ MDF Minh Long phủ melamine chống nước chống xước</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u v&acirc;n gỗ như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; bộ kệ tivi treo tường đơn giản, hiện đại vừa trang tr&iacute; vừa để được nhiều đồ ph&ugrave; hợp với khoảng tường rộng 220cm trở l&ecirc;n.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:31:17', 100),
(25, 14, 1, 'Kệ Tivi treo tường gỗ sơn trắng', 'SP-789334', '4000000.00', 0, 'Screenshot_13.png', NULL, 0, 3, '<h1>Kệ Tivi treo tường&nbsp;Deco TV81</h1>\r\n\r\n<p>M&atilde; sản phẩm : TV81</p>\r\n\r\n<p>Quy c&aacute;ch: bộ gồm kệ 3 ngăn k&eacute;o dưới tivi v&agrave; c&aacute;c khung kệ b&ecirc;n tr&ecirc;n v&agrave; xung quanh tivi như h&igrave;nh, phần kệ n&agrave;y s&acirc;u 12cm.( kh&ocirc;ng bao gồm đồ tr&ecirc;n kệ v&agrave; c&aacute;c chi tiết trang tr&iacute; d&aacute;n tr&ecirc;n tường)</p>\r\n\r\n<p>K&iacute;ch thước: Kệ 3 ngăn k&eacute;o dưới tivi d&agrave;i 180cm cao 20cm s&acirc;u 30cm , c&aacute;c kệ xung quanh tivi s&acirc;u 12cm</p>\r\n\r\n<p>Chất liệu : Kệ l&agrave;m từ gỗ MDF Minh Long phủ melamine rồi l&agrave;m tinh v&agrave; sơn phủ cao cấp, nước sơn tươi s&aacute;ng &iacute;t ngả m&agrave;u</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u trắng kết hợp đen như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Đ&acirc;y l&agrave; bộ kệ tivi treo tường kh&aacute; nhiều chi tiết nhưng gọn g&agrave;ng kiểu d&aacute;ng hiện đại sang trọng. Set kệ n&agrave;y ph&ugrave; hợp với khoảng tường tương đối rộng, khoảng tr&ecirc;n 2.5m.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:33:02', 100),
(26, 14, 1, 'Kệ Tivi treo tường đỏ Deco TV180', 'SP-679026', '2300000.00', 0, 'Screenshot_14.png', NULL, 0, 5, '<p>Trong kh&ocirc;ng gian ph&ograve;ng kh&aacute;ch Kệ tivi treo tường Deco TV180 g&oacute;p phần l&agrave;m kh&ocirc;ng gian trở n&ecirc;n gọn g&agrave;ng v&agrave; hiện đại sang trọng hơn.Chốn đ&ocirc; thị kh&ocirc;ng cho ph&eacute;p bạn bố tr&iacute; c&aacute;c đồ nội thất kh&ocirc;ng khoa học hợp l&yacute;. C&ograve;n g&igrave; tuyệt vời hơn khi căn ph&ograve;ng nhỏ c&oacute; thể chứa được nhiều sản phẩm hơn.</p>\r\n\r\n<p>M&atilde; sản phẩm : TV180</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm Kệ Tivi như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước: D&agrave;i 2m3 s&acirc;u 30cm cao 30cm gỗ 2,5cm</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước, phun sơn PU phủ b&oacute;ng</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:35:44', 100),
(27, 14, 1, 'Kệ Tivi treo tường gi Deco TV131', 'SP-340352', '3500000.00', 0, 'Screenshot_15.png', NULL, 0, 2, '<p>Ph&ograve;ng kh&aacute;ch vừa l&agrave; nơi ch&uacute;ng ta tiếp đ&oacute;n những vị kh&aacute;ch quan trọng vừa l&agrave; nơi để c&aacute;c th&agrave;nh vi&ecirc;n sum họp. Trong kh&ocirc;ng ng&ocirc;i nh&agrave; Kệ tivi treo tường Deco TV131 l&agrave; đồ nội thất kh&ocirc;ng thể thiếu, n&oacute; l&agrave;m tăng thẩm mỹ của căn ph&ograve;ng cũng như tiện lợi khi d&ugrave;ng.</p>\r\n\r\n<p>M&atilde; sản phẩm : TV131</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm Kệ Tivi như h&igrave;nh</p>\r\n\r\n<p>K&iacute;ch thước: D&agrave;i 2m cao 20cm s&acirc;u 24cm</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước, phủ Melamin</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:39:17', 100),
(28, 14, 1, 'Kệ tivi để sàn Deco DB96', 'SP-453495', '5000000.00', 0, 'Screenshot_16.png', NULL, 0, 0, '<h1>Kệ tivi để s&agrave;n&nbsp;Deco DB96</h1>\r\n\r\n<p>Kệ tivi đẹp để s&agrave;n Deco DB96 khiến kh&aacute;ch h&agrave;ng phải l&ograve;ng bởi n&eacute;t đẹp tinh tế pha ch&uacute;t sang trọng của sản phẩm. Kh&ocirc;ng kh&oacute; để đặt sản phẩm v&agrave;o kh&ocirc;ng gian sinh hoạt của mỗi gia đ&igrave;nh v&igrave; thiết kế mang phong c&aacute;ch hiện đại của sản phẩm rất dễ phối hợp.</p>\r\n\r\n<p>M&atilde; sản phẩm : DB96</p>\r\n\r\n<p>Quy c&aacute;ch: Gồm 1 kệ tivi để s&agrave;n như h&igrave;nh (chưa t&iacute;nh c&aacute;c sản phậm phụ xung quanh)</p>\r\n\r\n<p>K&iacute;ch thước(D x R x C): 125-200 x 30 x 30 cm Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long phủ l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước, phun sơn</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:41:21', 100),
(29, 11, 1, 'Giường Ngủ Kiểu Nhật Deco DGN04', 'SP-063053', '18000000.00', 0, 'Screenshot_17.png', NULL, 0, 25, '<h1>Giường Ngủ Kiểu Nhật&nbsp;Deco DGN04</h1>\r\n\r\n<p>Được thiết kế với kiểu d&aacute;ng tối giản, đường n&eacute;t trau chuốt, tinh tế, mẫu giường ngủ phong c&aacute;ch Nhật Bản Deco DGN04 đang l&agrave; lựa chọn của nhiều kh&aacute;ch h&agrave;ng khi đến với Xưởng Nội Thất Deco.</p>\r\n\r\n<p>M&atilde; sản phẩm :DGN04</p>\r\n\r\n<p>Quy c&aacute;ch: Bộ gồm 1 giường ngủ phong c&aacute;ch Nhật Bản, 2 tab 2 b&ecirc;n, đệm đầu giường như h&igrave;nh (chưa t&iacute;nh đệm nằm)</p>\r\n\r\n<p>K&iacute;ch thước : Thường l&agrave;m k&iacute;ch thước ph&ugrave; hợp với k&iacute;ch thước đệm 1m6x2m hoặc 1m8x2m hoặc bạn cũng c&oacute; thể đặt k&iacute;ch thước theo nhu cầu của m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : gỗ sồi tự nhi&ecirc;n</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc m&agrave;u theo y&ecirc;u cầu của bạn để ph&ugrave; hợp với m&agrave;u nội thất hiện c&oacute; trong ph&ograve;ng. Bạn vui l&ograve;ng li&ecirc;n hệ với Deco để trao đổi trực tiếp.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n\r\n<h2>Những ưu điểm của Giường Ngủ Kiểu Nhật Deco DGN04</h2>\r\n\r\n<p>Giường ngủ đẹp Deco DGN04 thiết kế tinh tế, thanh lịch, tối giản v&agrave; trau chuốt từng đường n&eacute;t theo phong c&aacute;ch Nhật Bản</p>\r\n\r\n<p>C&oacute; nhiều m&agrave;u sắc v&agrave; k&iacute;ch thước đa dạng cho kh&aacute;ch h&agrave;ng lựa chọn</p>\r\n\r\n<p>Chất lượng sản phẩm vượt trội, đ&aacute;p ứng mọi nhu cầu sử dụng</p>\r\n\r\n<p>Sản xuất tại xưởng tiết kiệm chi ph&iacute;, gi&aacute; th&agrave;nh phải chăng, ch&iacute;nh s&aacute;ch giao h&agrave;ng, lắp đặt tận nh&agrave;</p>\r\n\r\n<p>Bảo h&agrave;nh 24 th&aacute;ng</p>\r\n', '2020-10-24 10:43:25', 95),
(30, 14, 1, 'Kệ tivi gỗ để sàn Deco DB85', 'SP-762618', '8000000.00', 10, 'Screenshot_18.png', NULL, 0, 3, '<h1>Kệ tivi để s&agrave;n&nbsp;Deco DB85</h1>\r\n\r\n<p>Kh&ocirc;ng chỉ đ&aacute;p ứng nhu cầu sử dụng của c&aacute;c gia đ&igrave;nh, sự xuất hiện của mẫu Kệ tivi đẹp để s&agrave;n Deco DB85 c&ograve;n gi&uacute;p tăng t&iacute;nh thẩm mỹ, tạo điểm nhấn b&ecirc;n trong căn ph&ograve;ng. B&ecirc;n cạnh c&aacute;c mẫu kệ tivi đẹp th&igrave; tại Xưởng nội thất Deco bạn c&ograve;n t&igrave;m thấy rất nhiều sản phẩm nội thất l&yacute; tưởng cho ph&ograve;ng kh&aacute;ch gia đ&igrave;nh như tủ trang tr&iacute;, gi&aacute; s&aacute;ch, b&agrave;n tr&agrave;&hellip;</p>\r\n\r\n<p>M&atilde; sản phẩm : DB85</p>\r\n\r\n<p>Quy c&aacute;ch: Gồm 1 kệ tivi để s&agrave;n như h&igrave;nh (chưa t&iacute;nh c&aacute;c sản phậm phụ xung quanh)</p>\r\n\r\n<p>K&iacute;ch thước(D x R x C): 180 x 30 x 45 cm Bạn cũng c&oacute; thể đặt k&iacute;ch thước y&ecirc;u cầu để ph&ugrave; hợp với kh&ocirc;ng gian nh&agrave; m&igrave;nh.</p>\r\n\r\n<p>Chất liệu : Gỗ MDF Minh Long phủ l&otilde;i xanh chống ẩm, ti&ecirc;u chuẩn ch&acirc;u &Acirc;u E1 về bảo vệ sức khỏe, chống nước chống xước, phủ melamin, ch&acirc;n sắt sơn tĩnh điện</p>\r\n\r\n<p>M&agrave;u sắc: M&agrave;u như h&igrave;nh hoặc bạn c&oacute; thể đặt m&agrave;u v&agrave; k&iacute;ch thước t&ugrave;y theo nhu cầu v&agrave; sở th&iacute;ch của m&igrave;nh.</p>\r\n\r\n<p>Thời gian giao h&agrave;ng: Từ 1 đến 7 ng&agrave;y sau khi đặt h&agrave;ng (Giao tới tận nh&agrave;)</p>\r\n', '2020-10-24 10:44:49', 95);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `user_email` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `user_phone` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `user_address` varchar(255) COLLATE utf8_bin NOT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `payment` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `payment_info` text COLLATE utf8_bin DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_bin DEFAULT '',
  `scode` char(30) COLLATE utf8_bin NOT NULL,
  `discount_code` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `statuss` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_name`, `user_email`, `user_phone`, `user_address`, `amount`, `payment`, `payment_info`, `message`, `scode`, `discount_code`, `statuss`, `created`) VALUES
(1, 'Lê Văn Tuấn', 'giapminhhoang98@gmail.com', '0987789788', 'Hà Nội', '24000000.00', 'Thanh toán trực tiếp', NULL, '', '97601', '', 0, '2020-10-28 23:12:59'),
(2, 'Trịnh Trung Hiếu', 'giapminhhoang98@gmail.com', '0987654321', 'Đống Đa, Hà Nội', '32000000.00', 'Thanh toán khi nhận hàng', NULL, '', '38728', '', 0, '2020-10-28 23:22:03'),
(3, 'Trần Văn Vũ', 'minhhoangj1998@gmail.com', '0898989898', 'Hà Nội', '25800000.00', 'Thanh toán trực tiếp', NULL, 'Test', '74296', '', 0, '2020-10-28 23:24:34'),
(4, 'Giáp Minh Hoàng', 'giapminhhoang98@gmail.com', '0987789788', 'số nhà 21, ngõ 462/35/1 đường bưởi hà nội', '30000000.00', 'Thanh toán trực tiếp', NULL, '', '95303', '', 2, '2020-12-07 21:39:58'),
(5, 'Giáp Minh Hoàng', 'giapminhhoang98@gmail.com', '0898989898', 'số nhà 21, ngõ 462/35/1 đường bưởi hà nội', '236000000.00', 'Thanh toán trực tiếp', NULL, '', '92341', '', 0, '2020-12-07 21:46:29'),
(6, 'Giáp Minh Hoàng', 'giapminhhoang98@gmail.com', '0989898989', 'số nhà 21, ngõ 462/35/1 đường bưởi hà nội', '90000000.00', 'Thanh toán trực tiếp', NULL, '', '55525', '', 2, '2020-12-07 21:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_detail`
--

CREATE TABLE `transactions_detail` (
  `id` int(11) NOT NULL,
  `transactions_id` int(11) DEFAULT NULL,
  `products_code` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `price` decimal(15,2) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transactions_detail`
--

INSERT INTO `transactions_detail` (`id`, `transactions_id`, `products_code`, `price`, `qty`, `amount`) VALUES
(1, 1, 'SP-789334', '4000000.00', 1, '4000000.00'),
(2, 1, 'SP-473715', '20000000.00', 1, '20000000.00'),
(3, 2, 'SP-764143', '32000000.00', 1, '32000000.00'),
(4, 3, 'SP-340352', '3500000.00', 1, '3500000.00'),
(5, 3, 'SP-679026', '2300000.00', 1, '2300000.00'),
(6, 3, 'SP-473715', '20000000.00', 1, '20000000.00'),
(7, 4, 'SP-301560', '3000000.00', 10, '30000000.00'),
(8, 5, 'SP-473715', '20000000.00', 10, '200000000.00'),
(9, 5, 'SP-762618', '7200000.00', 5, '36000000.00'),
(10, 6, 'SP-063053', '18000000.00', 5, '90000000.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `phone` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin NOT NULL,
  `created` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `address`, `created`) VALUES
(9, 'Người dùng', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '0949189278', 'Hòa Bình', '2019-07-29 09:46:27'),
(10, 'Người dùng 1', 'nguoidung1@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '0998898989', 'Sơn La', '2019-09-22 15:46:00'),
(11, 'Người dùng 2', 'nguoidung2@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '0987654321', 'Hà Nội', '2020-10-17 15:47:55'),
(12, 'Người dùng 3', 'nguoidung3@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '0990909090', 'Ba Đình Hà Nội', '2020-10-17 13:27:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `design_catalog`
--
ALTER TABLE `design_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `design_catalog`
--
ALTER TABLE `design_catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transactions_detail`
--
ALTER TABLE `transactions_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
