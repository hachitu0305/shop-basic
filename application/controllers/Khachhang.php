<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Khachhang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Khachhang_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if (!empty($_SESSION['username'])) {
			
			$total_rows = count($this->Khachhang_Model->get());
			$per_page = 10;


			$this->load->library('pagination');

			$config['base_url'] = base_url().'Hang/index';;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = 3;
			$config['num_links'] = 3;

			$config['num_tag_open'] = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';


			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';


			$config['cur_tag_open'] = '<li class="page-item page-link" style="border-color:#17a2b8;">';
			$config['cur_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = $this->pagination->create_links();

			$uri_seg = $this->uri->segment(3);

			$data['all']=$this->Khachhang_Model->getLimit($per_page,$uri_seg);
			$data['page'] = $page;

			$this->load->view('admin_views/khachhang_view',$data);
		}

		else {

			redirect('Admin','refresh');

		}
		
	}

	public function multidel()
	{
		if(!empty($_SESSION['username'])) {

			$ids = $this->input->post('checked_id');

			if ($ids != NULL) {
				
				$res = $this->Khachhang_Model->multidelete($ids);

				if ($res) {

					$this->session->set_flashdata('users_su','Thao tác thành công !!!');
					$this->session->set_flashdata('users_er','');
					$this->session->set_flashdata('users_wr','');

				}

				else {


					$this->session->set_flashdata('users_su','');
					$this->session->set_flashdata('users_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('users_wr','');

				}
			}

			else {
				
				$this->session->set_flashdata('users_su','');
				$this->session->set_flashdata('users_er','');
				$this->session->set_flashdata('users_wr','Chưa chọn các bản ghi cần xóa !!!');

			}

			redirect('Khachhang','refresh');
		}

		else {

			$this->index();
		}
	}

	public function send_ads()
	{
		$data = $this->input->post();

		$all_user = $this->Khachhang_Model->get();

		try {

			foreach ($all_user as $user) {

				$this->send_mail($data['sub'],$data['cont'],$user['email']);


				$this->session->set_flashdata('users_su','Thao tác thành công !!!');
				$this->session->set_flashdata('users_er','');
				$this->session->set_flashdata('users_wr','');
			}
			
		} catch (Exception $e) {
			
			$this->session->set_flashdata('users_su','');
			$this->session->set_flashdata('users_er','Thao tác thất bại !!!');
			$this->session->set_flashdata('users_wr','');
		}

		redirect('Khachhang','refresh');
		
	}

	public function load_logincustomer()
	{
		$this->load->view('site_views/dangnhap_view');
	}

	public function logincustomer()
	{
		$data = $this->input->post();
		$pass = md5($data['password']);

		$cus = $this->Khachhang_Model->login($data['email'] , $pass );

		if (count($cus) != 0) {
			
			echo '<script type="text/javascript" charset="utf-8">
			alert("Đăng nhập thành công !!!");
			</script>';

			$_SESSION['customer'] = $data['email'];
			$_SESSION['id'] = $cus[0]['id'];
		}

		else {
			
			echo '<script type="text/javascript" charset="utf-8">
			alert("Đăng nhập thất bại !!!");
			</script>';
		}

		redirect('Trangchu','refresh');
	}

	public function logoutcustomer()
	{
		$this->session->unset_userdata("customer");
		$this->session->unset_userdata("id");
		redirect('Trangchu','refresh');
	}

	public function load_register()
	{
		$this->load->view('site_views/dangky_view');
	}

	public function register()
	{
		$data = $this->input->post();

		if ($data['code'] == $_SESSION['code']) {
			
			$pass = md5($data['password']);

			$item = 
			[
				'name' => $data['name'],
				'email' => $data['email'],
				'password' => $pass,
				'phone' => $data['phone'],
				'address' => $data['address']
			];

			if ($this->Khachhang_Model->insert($item)) {

				echo '<script  type="text/javascript" charset="utf-8" >
				alert("Tạo tài khoản thành công !!! Vui lòng đăng nhập");
				</script>';

			}

			else {
				
				echo '<script  type="text/javascript" charset="utf-8" >
				alert("Tạo tài khoản thất bại");
				</script>';
			}
		}

		else {
			
			echo '<script  type="text/javascript" charset="utf-8" >
			alert("Vui lòng kiểm tra lại mã xác nhận");
			</script>';
		}

		redirect('Khachhang/load_register','refresh');

		$this->session->unset_userdata('code');
		
	}

	public function send()
	{
		$email = $this->input->post('email');

		$mess = '';
		$code = $this->rand_string(5);

		$_SESSION['code'] = $code;

		$mess .='Mã xác thực của bạn là : '.$code;

		try {

			$this->send_mail('Xin chào !!!',$mess,$email);

			echo 'Vui lòng kiểm tra email !!!!';
			
		} 
		catch (Exception $e) {

			print 'error';
		}

	}

	public function rand_string( $length ) 
	{
		$str='';
		$chars = "0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}

	public function send_mail($subject,$mess,$to)
	{
		$this->load->library('email');
		//0

		$mail_config['smtp_host'] = 'smtp.gmail.com';
		$mail_config['smtp_port'] = '587';
		$mail_config['smtp_user'] = 'acc.send.mail.hoang@gmail.com';
		$mail_config['_smtp_auth'] = TRUE;
		$mail_config['smtp_pass'] = 'nyqqwmzwbknjdawz';
		$mail_config['smtp_crypto'] = 'tls';
		$mail_config['protocol'] = 'smtp';
		$mail_config['mailtype'] = 'html';
		$mail_config['send_multipart'] = FALSE;
		$mail_config['charset'] = 'utf-8';
		$mail_config['wordwrap'] = TRUE;

		$this->email->initialize($mail_config);

		$this->email->set_newline("\r\n");

		$this->email->from($mail_config['smtp_user'], 'NoithatAMD.com');
		$this->email->to($to);

        // $this->email->cc('another@example.com');
        // $this->email->bcc('and@another.com');

		$this->email->subject($subject);
		$this->email->message($mess);

		$this->email->send();
	}

}

/* End of file Khachhang.php */
/* Location: ./application/controllers/Khachhang.php */
