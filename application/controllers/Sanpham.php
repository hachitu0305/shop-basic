<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sanpham extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Sanpham_Model');
		$this->load->model('Danhmuc_Model');
		$this->load->model('Hang_Model');

	}

	// List all your items
	public function index( $offset = 0 )
	{ 
		if (!empty($_SESSION['username'])) {

			$total_rows = count($this->Sanpham_Model->get());
			$per_page = 10;


			$this->load->library('pagination');

			$config['base_url'] = base_url().'Sanpham/index';;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = 3;
			$config['num_links'] = 3;

			$config['num_tag_open'] = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';


			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';


			$config['cur_tag_open'] = '<li class="page-item page-link" style="border-color:#17a2b8;">';
			$config['cur_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = $this->pagination->create_links();

			$uri_seg = $this->uri->segment(3);

			$data['all'] = $this->Sanpham_Model->getLimit($per_page,$uri_seg);
			$data['cats'] = $this->Danhmuc_Model->get();
			$data['coms'] = $this->Hang_Model->get();
			$data['page'] = $page;

			$this->load->view('admin_views/sanpham_view',$data);
		}

		else {

			redirect('Admin','refresh');
		}
	}

	public function open_insertform()
	{
		if(!empty($_SESSION['username'])){

			$data['cats'] = $this->Danhmuc_Model->get();
			$data['coms'] = $this->Hang_Model->get();

			$this->load->view('admin_views/themsp_view',$data);
		}
		else{

			$this->index();
		}
		
	}

	// Add a new item
	public function add()
	{
		if(!empty($_SESSION['username'])) {

			$data = $this->input->post();

			$bar = $this->rand_string(6);
			$code = 'SP-'.$bar;

			$item = 
			[

				'catalog_id' => $data['danhmuc'],
				'com_id' => $data['hang'],
				'name' => $data['tensp'],
				'code' => $code,
				'price' => $data['gia'],
				'discount' => $data['giamgia'],
				'img_link' => $data['anh'],
				'detail' => $data['chitiet'],
				'item_count' => ($data['soluonght']<0?0:$data['soluonght'])
			];

			$res = $this->Sanpham_Model->insert($item);

			if ($res) {


				$this->session->set_flashdata('sp_su','Thao tác thành công !!!');
				$this->session->set_flashdata('sp_er','');

				redirect('Sanpham','refresh');

			}
			else {

				$this->session->set_flashdata('sp_su','');
				$this->session->set_flashdata('sp_er','Thao tác thất bại !!!');

				redirect('Sanpham','refresh');
			}

			
		}

		else {

			$this->index();
		}
	}

	public function detailitem($id)
	{
		if (!empty($_SESSION['username'])) {
			
			$data['cats'] = $this->Danhmuc_Model->get();
			$data['coms'] = $this->Hang_Model->get();

			$data['prod'] = $this->Sanpham_Model->getbyid($id);

			$this->load->view('admin_views/suasp_view',$data);
		}
		else {

			$this->index();
		}
		
	}

	//Update one item
	public function update()
	{
		if(!empty($_SESSION['username'])) {

			$data = $this->input->post();

			$item = 
			[

				'catalog_id' => $data['danhmuc'],
				'com_id' => $data['hang'],
				'name' => $data['tensp'],
				'price' => $data['gia'],
				'discount' => $data['giamgia'],
				'img_link' => $data['anh'],
				'detail' => $data['chitiet'],
				'item_count' => ($data['soluonght']<0?0:$data['soluonght'])
			];

			$res = $this->Sanpham_Model->update($item,$data['id']);

			if ($res) {


				$this->session->set_flashdata('sp_su','Thao tác thành công !!!');
				$this->session->set_flashdata('sp_er','');

				redirect('Sanpham','refresh');

			}
			else {

				$this->session->set_flashdata('sp_su','');
				$this->session->set_flashdata('sp_er','Thao tác thất bại !!!');

				redirect('Sanpham','refresh');

			}

			
		}

		else {

			$this->index();
		}
	}

	//Delete one item
	public function delete( $id = NULL )
	{

		if (!empty($_SESSION['username'])) {
			
			$id = $this->input->post('id');

			$res = $this->Sanpham_Model->delete($id);

			if ($res) {

				$this->session->set_flashdata('sp_su','Thao tác thành công !!!');
				$this->session->set_flashdata('sp_er','');

			}

			else {


				$this->session->set_flashdata('sp_su','');
				$this->session->set_flashdata('sp_er','Thao tác thất bại !!!');

			}

			redirect('Sanpham','refresh');
		}

		else {
			
			$this->index();
		}


	}

	public function multidel()
	{

		if (!empty($_SESSION['username'])) {
			
			$ids = $this->input->post('checked_id');

			if ($ids != NULL) {
				
				$res = $this->Sanpham_Model->multidelete($ids);

				if ($res) {

					$this->session->set_flashdata('sp_su','Thao tác thành công !!!');
					$this->session->set_flashdata('sp_er','');
					$this->session->set_flashdata('sp_wr','');

				}

				else {


					$this->session->set_flashdata('sp_su','');
					$this->session->set_flashdata('sp_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('sp_wr','');

				}
			}

			else {
				
				$this->session->set_flashdata('sp_su','');
				$this->session->set_flashdata('sp_er','');
				$this->session->set_flashdata('sp_wr','Chưa chọn các bản ghi cần xóa !!!');
			}

			redirect('Sanpham','refresh');
		}

		else {
			
			$this->index();
		}
		
	}

	public function filter_danhmuc()
	{
		$id = $this->input->post('id');

		$data = $this->Sanpham_Model->getbycatalog($id);

		echo $data;
	}

	public function filter_hang()
	{
		$id = $this->input->post('id');

		$data = $this->Sanpham_Model->getbycom($id);

		echo $data;
	}

	public function uploadimg()
	{
		if (isset($_POST) && !empty($_FILES['file'])) {

			$duoi = explode('.', $_FILES['file']['name']); 
			$duoi = $duoi[(count($duoi) - 1)];

			if ($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif'|| $duoi === 'jpeg') {

				if (move_uploaded_file($_FILES['file']['tmp_name'], './assets/uploads/' . $_FILES['file']['name'])) {

					die($_FILES['file']['name']); 
				} 
				else { 
					die('Có lỗi!'); 
				}
			}
			else { 
				die('Chỉ được upload ảnh'); 
			}
		} 
		else {
			die('Lock'); 
		}

	}

	public function rand_string( $length ) {
		$str='';
		$chars = "0123456789";
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}
}

/* End of file Sanpham.php */
/* Location: ./application/controllers/Sanpham.php */
