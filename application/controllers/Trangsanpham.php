<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trangsanpham extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Sanpham_Model');
		$this->load->model('Danhmuc_Model');
		$this->load->model('Hang_Model');
	}

	public function index()
	{
		$total_rows = count($this->Sanpham_Model->get());
		$per_page = 15;


		$this->load->library('pagination');

		$config['base_url'] = base_url().'Trangsanpham/index';;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';


		$config['next_link'] = '»';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '«';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';


		$config['cur_tag_open'] = '<li>';
		$config['cur_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$page = $this->pagination->create_links();

		$uri_seg = $this->uri->segment(3);

		$data['all'] = $this->Sanpham_Model->getLimit($per_page,$uri_seg);
		$data['cats'] = $this->Danhmuc_Model->get();
		$data['coms'] = $this->Hang_Model->get();
		$data['page'] = $page;

		$this->load->view('site_views/sanphams_view',$data);
	}

	public function getdetail($code)
	{
		$detail = $this->Sanpham_Model->getbycode($code);
        

        $view_count = ($detail[0]['count_view'] + 1);
        $view = ['count_view' => $view_count];
        $this->Sanpham_Model->update($view , $detail[0]['id']);

		$data['detail'] = $detail;
		$data['same'] = $this->Sanpham_Model->get_item_samecat($detail[0]['cat_id'],$detail[0]['id']);

		$this->load->view('site_views/ctsanpham_view', $data);
	}

	public function filter()
	{
		$cat = $this->input->post('cat');
		$com = $this->input->post('com');

		$filter = $this->Sanpham_Model->getby_catcom($cat,$com);

		echo $filter;
	}

	public function search()
	{
		$text = $this->input->post('search');
		$total_rows = count($this->Sanpham_Model->get());
		$per_page = 15;


		$this->load->library('pagination');

		$config['base_url'] = base_url().'Trangsanpham/index';;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] = 3;
		$config['num_links'] = 5;

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';


		$config['next_link'] = '»';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '«';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';


		$config['cur_tag_open'] = '<li>';
		$config['cur_tag_close'] = '</li>';

		$this->pagination->initialize($config);

		$page = $this->pagination->create_links();

		$uri_seg = $this->uri->segment(3);

		$data['all'] = $this->Sanpham_Model->getLimit($per_page,$uri_seg,$text);
		$data['cats'] = $this->Danhmuc_Model->get();
		$data['coms'] = $this->Hang_Model->get();
		$data['page'] = $page;

		$this->load->view('site_views/sanphams_view',$data);
		
	}

}

/* End of file Trangsanpham.php */
/* Location: ./application/controllers/Trangsanpham.php */