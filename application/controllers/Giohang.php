<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Giohang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Sanpham_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		$this->load->view('site_views/giohang_view');
	}

	public function addto_cart($code)
	{
		$item['product'] = $this->Sanpham_Model->getbycode($code);
		$qty=$this->input->post('qty');
		$qty = $qty > 0 ? $qty : 1;

		if($item['product'][0]['item_count'] < $qty){
			echo '<script type="text/javascript" charset="utf-8" async defer>
			alert("Số lượng hàng không đủ");
			</script>';
			redirect('Trangsanpham/getdetail/'.$code,'refresh');
			return;
		}

		$price= $this->Cal_price($item['product'][0]['price'],$item['product'][0]['discount']);
		$name = $item['product'][0]['name'];
		$img = $item['product'][0]['img_link'];
		$data = array(
			'id'      => $code,
			'qty'     => $qty,
			'price'   => $price,
			'name'    => $name,
			'img'     =>$img
		);
		
		$chk = $this->cart->insert($data);

		redirect('Giohang','refresh');
	}

	public function update_cart($proId)
	{
		$id=$this->input->post('num-id');
		$number=$this->input->post('num-order');

		$product = $this->Sanpham_Model->getbycode($proId);		

		if($product[0]['item_count'] < $number){
				echo '<script type="text/javascript" charset="utf-8" async defer>
				alert("Số lượng hàng không đủ");
				</script>';
				redirect('Giohang','refresh');
				return;
			}

		if($number > 0) {

			$data = array(
				'rowid' => $id,
				'qty'   => $number
			);

			$update=$this->cart->update($data);
		}

		else {

			$data = array(
				'rowid' => $id,
				'qty'   => 0
			);

			$update=$this->cart->update($data);
		}

		redirect('Giohang','refresh');
	}

	public function removeall()
	{
		$this->cart->destroy();

		redirect('Giohang','refresh');
	}

	public function Cal_price($price , $discount)
	{
		return round($price - ( ($discount * $price)/100 ));
	}


}

/* End of file Giohang.php */
/* Location: ./application/controllers/Giohang.php */
