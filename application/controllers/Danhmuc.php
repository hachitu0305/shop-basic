<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Danhmuc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->load->model('Danhmuc_Model');
	}

	// List all your items
	public function index( $offset = 0 )
	{
		if (!empty($_SESSION['username'])) {
			
			$data['all_parent']=$this->Danhmuc_Model->get_catalog_parent();


			$total_rows = count($this->Danhmuc_Model->get());
			$per_page = 10;


			$this->load->library('pagination');

			$config['base_url'] = base_url().'Danhmuc/index';;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = 3;
			$config['num_links'] = 3;

			$config['num_tag_open'] = '<li class="page-item page-link">';
			$config['num_tag_close'] = '</li>';


			$config['next_link'] = '»';
			$config['next_tag_open'] = '<li class="page-item page-link">';
			$config['next_tag_close'] = '</li>';

			$config['prev_link'] = '«';
			$config['prev_tag_open'] = '<li class="page-item page-link">';
			$config['prev_tag_close'] = '</li>';


			$config['cur_tag_open'] = '<li class="page-item page-link" style="border-color:#17a2b8;">';
			$config['cur_tag_close'] = '</li>';

			$this->pagination->initialize($config);

			$page = $this->pagination->create_links();

			$uri_seg = $this->uri->segment(3);

			$data['all']=$this->Danhmuc_Model->getLimit($per_page,$uri_seg);
			$data['alldata']=$this->Danhmuc_Model->get();
			$data['page'] = $page;

			$this->load->view('admin_views/danhmuc_view',$data);
		}

		else {

			redirect('Admin','refresh');
		}
		
		
	}

	public function multidel()
	{
		if (!empty($_SESSION['username'])) {

			$ids = $this->input->post('checked_id');

			if ($ids != NULL) {
				
				$res = $this->Danhmuc_Model->multidelete($ids);

				if ($res) {

					$this->session->set_flashdata('dm_su','Thao tác thành công !!!');
					$this->session->set_flashdata('dm_er','');
					$this->session->set_flashdata('dm_wr','');

				}

				else {


					$this->session->set_flashdata('dm_su','');
					$this->session->set_flashdata('dm_er','Thao tác thất bại !!!');
					$this->session->set_flashdata('dm_wr','');

				}
			}

			else {

				$this->session->set_flashdata('dm_su','');
				$this->session->set_flashdata('dm_er','');
				$this->session->set_flashdata('dm_wr','Chưa chọn các bản ghi cần xóa !!!');
			}
			

			redirect('Danhmuc','refresh');
		}

		else {

			$this->index();
		}
	}
	

	// Add a new item
	public function add()
	{
		if(!empty($_SESSION['username'])) {

			$data = $this->input->post();

			$item = 
			[

				'name' => $data['tendanhmuc'],
				'parent_id' => $data['danhmuccha'],
				'note' => $data['ghichu']

			];

			$res = $this->Danhmuc_Model->insert($item);

			if ($res) {


				$this->session->set_flashdata('dm_su','Thao tác thành công !!!');
				$this->session->set_flashdata('dm_er','');

			}
			else {

				$this->session->set_flashdata('dm_su','');
				$this->session->set_flashdata('dm_er','Thao tác thất bại !!!');
			}

			redirect('Danhmuc','refresh');
		}

		else {

			$this->index();
		}
	}

	//Update one item
	public function update( $id = NULL )
	{
		if (!empty($_SESSION['username'])) {
			
			$data = $this->input->post();

			$item = 
			[
				'name' => $data['tendanhmuc'], 
				'parent_id' => $data['danhmuccha'] , 
				'note' => $data['ghichu'] 
			];

			$res = $this->Danhmuc_Model->update($item,$data['id']);

			if ($res) {

				$this->session->set_flashdata('dm_su','Thao tác thành công !!!');
				$this->session->set_flashdata('dm_er','');

			}

			else {


				$this->session->set_flashdata('dm_su','');
				$this->session->set_flashdata('dm_er','Thao tác thất bại !!!');

			}

			redirect('Danhmuc','refresh');

		}

		else {
			
			$this->index();
		}
		
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		if (!empty($_SESSION['username'])) {
			
			$id=$this->input->post('id');

			$res = $this->Danhmuc_Model->delete($id);

			if ($res) {

				$this->session->set_flashdata('dm_su','Thao tác thành công !!!');
				$this->session->set_flashdata('dm_er','');

			}

			else {


				$this->session->set_flashdata('dm_su','');
				$this->session->set_flashdata('dm_er','Thao tác thất bại !!!');

			}

			redirect('Danhmuc','refresh');
		}

		else {

			$this->index();
		}
		

	}

	public function filter()
	{
		if (!empty($_SESSION['username'])) {
			
			$id = $this->input->post('pa_id');

			$data = $this->Danhmuc_Model->filter_parentcat($id);
			echo $data;
		}

		else {
			
			$this->index();
		}
		
	}
}

/* End of file Danhmuc.php */
/* note: ./application/controllers/Danhmuc.php */
