<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
	<div class="wrapper">


		<?php include 'navbar.php'; ?>

		<?php include 'sidebar.php' ?>


		<div class="content-wrapper" style="min-height: 823.896px;">

			
			<div class="row">
				<div class="col-md-8">
					<?php include 'header_content.php'; ?>
				</div>


				<div class="col-md-4" style="margin-top: 20px;">
					<div class="row">
						<button class="btn btn-primary" id="btn_submit" style="width: 150px;"
						onclick="submit_form();" type="button">
						Lưu
					</button>
					&#160;
					<a class="btn btn-secondary" href="<?= base_url() ?>Sanpham" style="width: 150px;">
						Trở về
					</a>
				</div>
			</div>
		</div>

		<section class="content">
			<div class="container-fluid">


				<!-- st: Alert -->
				<?php if ($this->session->flashdata('sp_er')): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

						<?= $this->session->flashdata('sp_er'); ?>

					</div>
				<?php endif ?>


				<?php if ($this->session->flashdata('sp_su')): ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fas fa-check"></i> Thành công !</h5>

						<?= $this->session->flashdata('sp_su'); ?>

					</div>
				<?php endif ?>

				<!-- end: Alert -->


				<!-- st: insertform -->

				<div class="card" style="height: auto;">
					<div class="card-header" style="background: #17a2b8;">
						<h3 class="card-title" style="color: white;">Thêm mới sản phẩm</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">

						<div class="row">

							<div class="col-md-4" style="width: auto; vertical-align: top;">
								<label>Ảnh hiển thị</label>
								<img src="<?= base_url() ?>assets/uploads/no_img.png" 
								alt="&#160;&#160;&#160;Không có ảnh !!!"
								style="width: 300px; height: 250px; margin-top: 0px;
								border-style: dotted; min-height: 250px; min-width: 360px;" 
								id="myimg">
								<input id="file" type="file" style="display:none;">
								<div style="height: 10px;"></div>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="openFileOption();">Chọn</button>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="upload();">Tải lên</button>
							</div>

							<div class="col-md-6" style="display: inline; margin: 0px 0px 0px 100px;">

								<form action="<?= base_url() ?>Sanpham/add" method="post" id="form_insert">

									<div style="height: 15px;"></div>

									<div class="col-md-3">
										<label>Tên danh mục</label>
									</div>
									<div class="col-md-12">
										<select name="danhmuc" class="form-control" style="width: 500px;"
										id="danhmuc">
										<option value="">-- Chọn danh mục --</option>
										<?php showCategories($cats) ?>
									</select>
								</div>

								<div style="height: 15px;"></div>

								<div class="col-md-3">
									<label>Loại thiết kế</label>
								</div>
								<div class="col-md-12">
									<select name="hang" class="form-control" style="width: 500px;"
									id="hang">
									<option value="">-- Chọn thiết kế --</option>
									<?php foreach ($coms as $com): ?>
										<option value="<?= $com['id'] ?>"><?= $com['name'] ?></option>
									<?php endforeach ?>
								</select>
							</div>

							<div style="height: 15px;"></div>


							<div class="col-md-3">
								<label>Tên sản phẩm</label>
							</div>
							<div class="col-md-12">
								<input name="tensp" id="tensp"
								type="text" class="form-control" style="width: 500px;"
								placeholder="Tên sản phẩm">
							</div>

							<input type="hidden" name="anh" id="anh">

							<div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Giá sản phẩm</label>
							</div>
							<div class="col-md-12">
								<input type="text" class="form-control" style="width: 500px; "
								placeholder="Giá sản phẩm" name="gia" id="gia">
							</div>

							<div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Giảm giá (%)</label>
							</div>
							<div class="col-md-12">
								<input type="text" class="form-control" style="width: 500px; "
								placeholder="Giảm giá" name="giamgia" id="giamgia">
							</div>

							<div style="height: 15px;"></div>

							<div class="col-md-4">
								<label>Số lượng hiện tại</label>
							</div>
							<div class="col-md-12">
								<input type="number" class="form-control" style="width: 500px; "
								min="0" value="0" 
								placeholder="Số lượng hiện tại" name="soluonght" id="soluonght">
							</div>

							<div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Chi tiết</label>
							</div>
							<div class="col-md-12">
								<textarea name="chitiet" id="chitiet" class="form-control"></textarea>
								<script>
									CKEDITOR.replace('chitiet');
								</script>
							</div>

						</form>

					</div>
				</div>

			</div>
			<!-- /.card-body -->

			<div class="card-footer clearfix" style="background: #17a2b8;">


			</div>

		</div>

		<!-- end:insertform -->

	</div><!-- /.container-fluid -->
</section>

</div>

<script type="text/javascript">

	function openFileOption(argument) {

		$('#file').click();
	};

</script>

<script type="text/javascript">

	function upload(argument) {

		var v = $('#file').val();

		alert(v);

		var path="<?= base_url() ?>";

		var file_data = $('#file').prop('files')[0];

		var type = file_data.type;


		var match = ["image/gif", "image/png", "image/jpg","image/jpeg"];

		if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {

			var form_data = new FormData();

			form_data.append('file', file_data);


			$.ajax({
				url: path+'Sanpham/uploadimg', 
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (res) {

					$('#file').val('');
					$('#myimg').attr('src', '<?= base_url() ?>assets/uploads/'+res);
					$('#anh').val(res); 
				}
			});
		}
		else {

			$('#file').val('');
		}
	}

</script>

<?php 

function showCategories($cats, $parent_id = 0, $char = '',$selected_id = 0)
{
	foreach ($cats as $item)
	{
          // Nếu là chuyên mục con thì hiển thị
		if ($item['parent_id'] == $parent_id)
		{
			if ($item['parent_id']==$selected_id && $selected_id != 0) {
				echo '<option selected value="'.$item['id'].'">';
				echo $char . $item['name'];
				echo '</option>';

			}
			else {
				echo '<option value="'.$item['id'].'">';
				echo $char . $item['name'];
				echo '</option>';
			}

              // Xóa chuyên mục đã lặp
			unset($cats['id']);

              // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
			showCategories($cats, $item['id'], $char.'---| ');
		}
	}
}

?>

<script type="text/javascript">

	function submit_form(argument) {

		var anh = $('#anh').val().trim();
		var danhmuc = $('#danhmuc').val().trim();
		var hang = $('#hang').val().trim();
		var tensp = $('#tensp').val().trim();
		var gia = $('#gia').val().trim();
		var giamgia = $('#giamgia').val().trim();
		var chitiet = CKEDITOR.instances['chitiet'].getData();

		if( anh != '' && danhmuc != '' && hang != '' && tensp != '' && gia != ''
			&& gia > 0 && chitiet != '') 
		{
			$('#form_insert').submit();
		}

		else 
		{
			if(anh == ''){ $('#myimg').css('border-color', 'red'); }
			else { $('#myimg').css('border-color', 'black'); }

			if(danhmuc == ''){ $('#danhmuc').addClass('is-invalid'); }
			else { $('#danhmuc').removeClass('is-invalid'); }

			if(hang == ''){ $('#hang').addClass('is-invalid'); }
			else { $('#hang').removeClass('is-invalid'); }

			if(tensp == ''){ $('#tensp').addClass('is-invalid'); }
			else { $('#tensp').removeClass('is-invalid'); }

			if(gia == '' || gia <= 0){ $('#gia').addClass('is-invalid'); }
			else { $('#gia').removeClass('is-invalid'); }

			if(giamgia != '' && giamgia <= 0){ $('#gia').addClass('is-invalid'); }
			else { $('#gia').removeClass('is-invalid'); }
		}

	}

</script>

<?php include 'footer.php'; ?>
