<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
	<div class="wrapper">


		<?php include 'navbar.php'; ?>

		<?php include 'sidebar.php' ?>


		<div class="content-wrapper" style="min-height: 823.896px;">

			
			<div class="row">
				<div class="col-md-8">
					<?php include 'header_content.php'; ?>
				</div>


				<div class="col-md-4" style="margin-top: 20px;">
					<div class="row">
						<button class="btn btn-primary" id="btn_submit" style="width: 150px;"
						onclick="submit_form();" type="button">
						Lưu
					</button>
					&#160;
					<a class="btn btn-secondary" href="<?= base_url() ?>Tintuc" style="width: 150px;">
						Trở về
					</a>
				</div>
			</div>
		</div>

		<section class="content">
			<div class="container-fluid">


				<!-- st: Alert -->
				<?php if ($this->session->flashdata('sp_er')): ?>
					<div class="alert alert-danger alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

						<?= $this->session->flashdata('tintuc_er'); ?>

					</div>
				<?php endif ?>


				<?php if ($this->session->flashdata('sp_su')): ?>
					<div class="alert alert-success alert-dismissible">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h5><i class="icon fas fa-check"></i> Thành công !</h5>

						<?= $this->session->flashdata('tintuc_su'); ?>

					</div>
				<?php endif ?>

				<!-- end: Alert -->


				<!-- st: insertform -->

				<div class="card" style="height: auto;">
					<div class="card-header" style="background: #17a2b8;">
						<h3 class="card-title" style="color: white;">Thêm mới tin tức</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">

						<div class="row">
							<div class="col-md-4" style="width: auto; vertical-align: top;">
                            <label>Ảnh 1</label>
								<img src="<?= base_url() ?>assets/uploads/no_img.png" 
								alt="&#160;&#160;&#160;Không có ảnh !!!"
								style="width: 300px; height: 250px; margin-top: 0px;
								border-style: dotted; min-height: 250px; min-width: 360px;" 
								id="myimg">
								<input id="file" type="file" style="display:none;">
								<div style="height: 10px;"></div>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="openFileOption('#file');">Chọn</button>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="upload('#file','#myimg','#banner1');">Tải lên</button>
                                <!--  -->
                                <br>
                                <label style="margin-top: 15px;">Ảnh 2</label>
								<img src="<?= base_url() ?>assets/uploads/no_img.png" 
								alt="&#160;&#160;&#160;Không có ảnh !!!"
								style="width: 300px; height: 250px; margin-top: 0px;
								border-style: dotted; min-height: 250px; min-width: 360px;" 
								id="myimg1">
								<input id="file1" type="file" style="display:none;">
								<div style="height: 10px;"></div>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="openFileOption('#file1');">Chọn</button>
								<button type="btn_upload" class="btn btn-info btn-sm"
								onclick="upload('#file1','#myimg1','#banner2');">Tải lên</button>
							</div>

							<div class="col-md-6" style="display: inline; margin: 0px 0px 0px 100px;">

                            <form action="<?= base_url() ?>Tintuc/add" method="post" id="form_insert">

                            <div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Tiêu đề</label>
							</div>
							<div class="col-md-12">
								<input name="title" id="title"
								type="text" class="form-control" style="width: 500px;"
								placeholder="Tiêu đề">
							</div>

							<input type="hidden" name="banner1" id="banner1">
							<input type="hidden" name="banner2" id="banner2">

							<div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Mở đầu</label>
							</div>
							<div class="col-md-12">
								<textarea name="content" id="content" class="form-control"></textarea>
								<script>
									CKEDITOR.replace('content');
								</script>
							</div>

                            <div style="height: 15px;"></div>

							<div class="col-md-3">
								<label>Chi tiết</label>
							</div>
							<div class="col-md-12">
								<textarea name="desc" id="desc" class="form-control"></textarea>
								<script>
									CKEDITOR.replace('desc');
								</script>
							</div>

						</form>

					</div>
				</div>

			</div>
			<!-- /.card-body -->

			<div class="card-footer clearfix" style="background: #17a2b8;">


			</div>

		</div>

		<!-- end:insertform -->

	</div><!-- /.container-fluid -->
</section>

</div>

<script type="text/javascript">

	function openFileOption(idInput) {

		$(idInput).click();
	};

</script>

<script type="text/javascript">

	function upload(fileId,imgId,inputId) {

		var v = $(fileId).val();

		alert(v);

		var path="<?= base_url() ?>";

		var file_data = $(fileId).prop('files')[0];

		var type = file_data.type;


		var match = ["image/gif", "image/png", "image/jpg","image/jpeg"];

		if (type == match[0] || type == match[1] || type == match[2] || type == match[3]) {

			var form_data = new FormData();

			form_data.append('file', file_data);


			$.ajax({
				url: path+'Sanpham/uploadimg', 
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (res) {

					$(fileId).val('');
					$(imgId).attr('src', '<?= base_url() ?>assets/uploads/'+res);
					$(inputId).val(res); 
				}
			});
		}
		else {

			$(fileId).val('');
		}
	}

</script>

<script type="text/javascript">

	function submit_form(argument) {

		var banner1 = $('#banner1').val().trim();
		var banner2 = $('#banner2').val().trim();
		var title = $('#title').val().trim();
		var content = CKEDITOR.instances['content'].getData();
		var desc = CKEDITOR.instances['desc'].getData();

		if( banner1 != '' && banner2 != '' && title != '' && content != '' && desc != '') 
		{
			$('#form_insert').submit();
		}

		else 
		{
			if(banner1 == ''){ $('#myimg').css('border-color', 'red'); }
			else { $('#myimg').css('border-color', 'black'); }

			if(banner2 == ''){ $('#myimg1').css('border-color', 'red'); }
			else { $('#myimg1').css('border-color', 'black'); }

			if(title == ''){ $('#title').addClass('is-invalid'); }
			else { $('#title').removeClass('is-invalid'); }

			if(content == ''){ $('#content').addClass('is-invalid'); }
			else { $('#content').removeClass('is-invalid'); }

			if(desc == ''){ $('#desc').addClass('is-invalid'); }
			else { $('#desc').removeClass('is-invalid'); }
		}

	}

</script>

<?php include 'footer.php'; ?>
