<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>

     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <button type="button" class="btn btn-primary btn-round" 
        data-toggle="modal" data-target="#mail_modal">
        <i class="fa fa-paper-plane"></i>  &#160; Gửi mail
      </button>
      &#160;
      <button type="button" class="btn btn-danger btn-round"
      data-toggle="modal" data-target="#multidel">
      <i class="fas fa-trash-alt"></i>  &#160; Xóa
    </button>

  </div>
</div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('users_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('users_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('users_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('users_wr'); ?>

    </div>
  <?php endif ?>
  
  

  <?php if ($this->session->flashdata('users_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('users_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Khachhang/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng khách hàng  </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Họ tên</th>
                <th>E-mail</th>
                <th>Số điện thoại</th>
                <th>Địa chỉ</th>
                <th>Ngày tham gia</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>
                  <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td><?= $value['name'] ?></td>
                  <td><?= $value['email'] ?></td>
                  <td><?= $value['phone'] ?></td>
                  <td><?= $value['address'] ?></td>
                  <td><?= $value['created'] ?></td>
                </tr>

                <?php $i++; ?>
              <?php endforeach ?>

            </tbody></table>
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">

           <span id="count" class="badge badge-danger" style="font-size: 15px;">
             <?= $i; ?> bản ghi / trang </span>
             <ul class="pagination pagination-sm m-0 float-right">

               <?php echo $page; ?>

             </ul>
           </div>
         </div>

         <!-- end: table -->

       </form>

       <!-- /.card -->
     </div>
   </div><!-- /.row -->
 </div><!-- /.container-fluid -->
</section>

</div>

<!--st: add modal -->
<div class="modal fade" id="mail_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url() ?>Khachhang/send_ads" method="post" id="sendmail_form">

          <div class="form-group">
            <label for="sub">Tiêu đề thư</label>
            <input type="text" class="form-control" id="sub" 
            name="sub" placeholder="Tiêu đề thư">
          </div>
          
          <div class="form-group">
            <label for="cont">Nội dung thư</label>
            <textarea name="cont" id="cont" class="form-control"></textarea>
            <script>
              CKEDITOR.replace('cont');
            </script>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Submit()">Gửi</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>

      </form>

    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8" >

  function Submit(argument) {

   var sub = $('#sub').val().trim();
   var cont = CKEDITOR.instances['cont'].getData();

   if(sub != '' && cont != '')
   {
    $('#mail_modal').modal('hide');
    $('#sendmail_form').submit();
  }

  else
  {

    if(sub == ''){ $("#sub").addClass('is-invalid');} 
    else { $("#sub").removeClass('is-invalid');} 

  }

}

</script>

<!-- end: mail_modal -->



<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->


<?php include 'footer.php'; ?>
</div>