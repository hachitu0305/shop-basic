<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>


     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <div style="width: 150px;">
        </div>
        &#160;
        <button type="button" class="btn btn-danger btn-round"
        data-toggle="modal" data-target="#multidel">
        <i class="fas fa-trash-alt"></i>  &#160; Xóa
      </button>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('gg_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('gg_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('gg_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('gg_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('gg_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('gg_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Giaodich/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng giao dịch  </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Mã giao dịch</th>
                <th>Hình thức thanh toán</th>
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
                <th style="width: auto;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>
                  <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>

                  <td><?= $value['scode'] ?></td>
                  <td><?= $value['payment'] ?></td>
                  <?php if($value['statuss'] == 0) { ?>
                  <td><span class="badge badge-danger" style="font-size: 14px;"> Chưa xử lý </span></td>
                  <?php } elseif($value['statuss'] == 1) { ?>
                  <td><span class="badge badge-warning" style="font-size: 14px;"> Đã chuyển hàng </span></td>
                  <?php } else { ?>
                  <td><span class="badge badge-success" style="font-size: 14px;"> Đã thanh toán </span></td>
                  <?php } ?>
                  <td><?= $value['created'] ?></td>
                  <td>
                    <a title="Chi tiết" class="btn btn-sm btn-outline-info" 
                    href="<?= base_url() ?>Giaodich/detailitem/<?= $value['id'] ?>">
                    <i class="fas fa-pen"></i>
                  </a> 
                  | <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"
                  data-toggle="modal" data-target="#del<?= $value['id'] ?>">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>

            </tr>

            <?php $i++; ?>
          <?php endforeach ?>

        </tbody></table>
      </div>
      <!-- /.card-body -->
      <div class="card-footer clearfix">

       <span id="count" class="badge badge-danger" style="font-size: 15px;">
         <?= $i; ?> bản ghi / trang </span>
         <ul class="pagination pagination-sm m-0 float-right">

           <?php echo $page; ?>

         </ul>
       </div>
     </div>

     <!-- end: table -->

   </form>

   <!-- /.card -->
 </div>
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>

<!-- st: delete_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="del<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Giaodich/delete" method="post">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">
            <p>Xóa bản ghi này ????</p>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Xóa</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>

<!-- end: delete_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->


<?php include 'footer.php'; ?>
