<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>

     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <button type="button" class="btn btn-success btn-round" 
        data-toggle="modal" data-target="#add_modal">
        <i class="fas fa-plus"></i>  &#160; Thêm
      </button>
      &#160;
      <button type="button" class="btn btn-danger btn-round"
      data-toggle="modal" data-target="#multidel">
      <i class="fas fa-trash-alt"></i>  &#160; Xóa
    </button>
  </div>
</div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('user_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('user_er'); ?>

    </div>
  <?php endif ?>
  
  <?php if ($this->session->flashdata('user_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('user_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('user_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('user_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Taikhoan/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng quản trị viên  </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Tên đăng nhập</th>
                <th>Họ tên </th>
                <th>E-mail </th>
                <th style="width: 150px;">Trạng thái</th>
                <th style="width: auto;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>

                   <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>

                  <td><?= $value['username'] ?></td>
                  <td><?= $value['name'] ?></td>
                  <td><?= $value['email'] ?></td>

                  <?php if ($value['islogin'] == 1) { ?>
                  <td >
                    <label class="switch" title="On">
                      <input type="checkbox" checked="checked" disabled>
                      <span class="slider round"></span>
                    </label>
                    &#160; &#160; &#160; &#160;
                    <span class="badge badge-success" style="font-size: 15px;">On</span>
                  </td>
                  <?php } else { ?>
                  <td>
                   <label class="switch" title="Off">
                    <input type="checkbox" disabled>
                    <span class="slider round"></span>
                  </label>
                  &#160; &#160; &#160; &#160;
                  <span class="badge badge-warning" style="font-size: 15px;">Off</span>
                </td>
                <?php } ?>

                <td>
                  <button type="button" title="Chi tiết" class="btn btn-sm btn-outline-info" 
                  data-toggle="modal" data-target="#edit<?= $value['id'] ?>">
                  <i class="fas fa-pen"></i>
                </button> 
                | <button type="button" title="Xóa" class="btn btn-sm btn-outline-danger"
                data-toggle="modal" data-target="#del<?= $value['id'] ?>">
                <i class="fas fa-trash-alt"></i>
              </button>
            </td>
          </tr>

          <?php $i++; ?>
        <?php endforeach ?>

      </tbody></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">

     <span id="count" class="badge badge-danger" style="font-size: 15px;">
       <?= $i; ?> bản ghi / trang </span>
       <ul class="pagination pagination-sm m-0 float-right">

         <?php echo $page; ?>

       </ul>
     </div>
   </div>

   <!-- end: table -->

 </form>

 <!-- /.card -->
</div>
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>


<!--st: add modal -->
<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url() ?>Taikhoan/add" method="post" id="insert_form">

          <div class="form-group">
            <label for="name">Họ tên</label>
            <input type="text" class="form-control" id="name" 
            name="name" placeholder="Họ tên">
          </div>

          <div class="form-group">
            <label for="username">Tên đăng nhập</label>
            <input type="text" class="form-control" id="username"  
            name="username" placeholder="Tên đăng nhập"   >
          </div>

          <div class="form-group">
            <label for="password">Mật khẩu</label>
            <input type="password" class="form-control" id="password"
            name="password" placeholder="Mật khẩu" >
          </div>

          <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" id="email"  
            name="email" placeholder="E-mail" >
          </div>

          <div class="form-group">
            <label for="username">Số điện thoại</label>
            <input type="text" class="form-control" id="phone"  
            name="phone" placeholder="Số điện thoại" >
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Submit_insert()">Lưu</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>

      </form>

    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8" >

  function Submit_insert(argument) {

   var va_name = $('#name').val().trim();
   var va_username = $('#username').val().trim();
   var va_password = $('#password').val().trim();
   var va_email = $('#email').val().trim();
   var va_phone = $('#phone').val().trim();

   if(va_name != '' && va_password != '' && va_username != '' && va_phone != '')
   {
    $('#add_modal').modal('hide');
    $('#insert_form').submit();
  }

  else
  {

    if(va_name == ''){ $("#name").addClass('is-invalid');} 
    else { $("#name").removeClass('is-invalid');} 

    if(va_username == ''){ $("#username").addClass('is-invalid');} 
    else { $("#username").removeClass('is-invalid');} 

    if(va_password == ''){ $("#password").addClass('is-invalid');} 
    else { $("#password").removeClass('is-invalid');} 

    if(va_email == ''){ $("#email").addClass('is-invalid');} 
    else { $("#email").removeClass('is-invalid');} 

    if(va_phone == ''){ $("#phone").addClass('is-invalid');} 
    else { $("#phone").removeClass('is-invalid');} 

  }

}

</script>

<!-- end: add_modal -->

<!-- st: edit_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="edit<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Taikhoan/update" method="post" 
            id="update_form<?= $value['id'] ?>">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">

            <div class="form-group">
              <label for="name">Họ tên</label>
              <input type="text" class="form-control" id="name<?= $value['id'] ?>" 
              name="name" placeholder="Họ tên"   
              value="<?= $value['name'] ?>">
            </div>

            <input type="hidden" name="oldusername" value="<?= $value['username'] ?>">

            <div class="form-group">
              <label for="username">Tên đăng nhập</label>
              <input type="text" class="form-control" id="username<?= $value['id'] ?>"  
              name="username" placeholder="Tên đăng nhập"   
              value="<?= $value['username'] ?>">
            </div>

            <div class="form-group">
              <label for="password">Mật khẩu mới</label>
              <input type="password" class="form-control" id="password<?= $value['id'] ?>"  
              name="newpass" placeholder="Mật khẩu mới" >
            </div>

            <div class="form-group">
              <label for="email">E-mail</label>
              <input type="email" class="form-control" id="email<?= $value['id'] ?>"  
              name="email" placeholder="E-mail"   
              value="<?= $value['email'] ?>">
            </div>

            <div class="form-group">
              <label for="username">Số điện thoại</label>
              <input type="text" class="form-control" id="phone<?= $value['id'] ?>"  
              name="phone" placeholder="Số điện thoại"   
              value="<?= $value['phone'] ?>">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="Submit_update_<?= $value['id'] ?>()">Lưu</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

  <script type="text/javascript" charset="utf-8" >

    function Submit_update_<?= $value['id'] ?>(argument) {

     var va_name = $('#name<?= $value['id'] ?>').val().trim();
     var va_username = $('#username<?= $value['id'] ?>').val().trim();
     var va_email = $('#email<?= $value['id'] ?>').val().trim();
     var va_phone = $('#phone<?= $value['id'] ?>').val().trim();

     if(va_name != '' && va_username != '' && va_phone != '' && va_email != '')
     {
      $('#edit<?= $value['id'] ?>').modal('hide');
      $('#update_form<?= $value['id'] ?>').submit();
    }

    else
    {

      if(va_name == ''){ $("#name<?= $value['id'] ?>").addClass('is-invalid');}
      else { $("#name<?= $value['id'] ?>").removeClass('is-invalid'); } 

      if(va_username == ''){ $("#username<?= $value['id'] ?>").addClass('is-invalid');} 
      else { $("#username<?= $value['id'] ?>").removeClass('is-invalid'); }

      if(va_email == ''){ $("#email<?= $value['id'] ?>").addClass('is-invalid');} 
      else { $("#email<?= $value['id'] ?>").removeClass('is-invalid');} 

      if(va_phone == ''){ $("#phone<?= $value['id'] ?>").addClass('is-invalid');} 
      else { $("#phone<?= $value['id'] ?>").removeClass('is-invalid');} 

    }

  }

</script>

<?php endforeach ?>
<!-- end: edit_modal -->


<!-- st: delete_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="del<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form action="<?= base_url() ?>Taikhoan/delete" method="post">

            <input type="hidden" name="id" value="<?= $value['id'] ?>">
            <p>Xóa bản ghi này ????</p>

          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Xóa</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>

<!-- end: delete_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->


<?php include 'footer.php'; ?>
</div>