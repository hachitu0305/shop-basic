<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>

    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <div class="row">
      <div class="col-md-8">
       <?php include 'header_content.php'; ?>
     </div>


     <div class="col-md-4" style="margin-top: 20px;">
      <div class="row" >
        <button type="button" class="btn btn-success btn-round" 
        data-toggle="modal" data-target="#add_modal">
        <i class="fas fa-plus"></i>  &#160; Thêm
      </button>
      &#160;
      <button type="button" class="btn btn-danger btn-round"
      data-toggle="modal" data-target="#multidel">
      <i class="fas fa-trash-alt"></i>  &#160; Xóa
    </button>
  </div>
</div>
</div>

<section class="content">
  <div class="container-fluid">

   <!-- st: Alert -->
   <?php if ($this->session->flashdata('sale_er')): ?>
     <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Xảy ra lỗi !</h5>

      <?= $this->session->flashdata('sale_er'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('sale_wr')): ?>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-exclamation-triangle"></i> Cảnh báo !</h5>
      
      <?= $this->session->flashdata('sale_wr'); ?>

    </div>
  <?php endif ?>

  <?php if ($this->session->flashdata('sale_su')): ?>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-check"></i> Thành công !</h5>

      <?= $this->session->flashdata('sale_su'); ?>

    </div>
  <?php endif ?>

  <!-- end: Alert -->



  <div class="row">
    <div class="col-12">
      <form  id="myform" action="<?= base_url() ?>Giamgia/multidel" method="post">

        <!-- st:table -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Bảng mã giảm giá  </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="table" class="table table-bordered" style="border-radius: 6px;">
              <thead>
                <th style="width: 10px">Chọn</th>
                <th>Mã giảm giá</th>
                <th>Phần trăm giảm (%)</th>
                <th>Tình trạng</th>
                <th style="text-align: center;">Tác vụ</th>
              </thead>
              <tbody id="mytable">

                <?php $i = 0; ?>

                <?php foreach ($all as $value): ?>

                 <tr>

                   <td align="center">
                    <label class="container-check">
                      <input type="checkbox"  name="checked_id[]" value="<?= $value['id'] ?>">
                      <span class="checkmark"></span>
                    </label>
                  </td>
                  <td><?= $value['id'] ?></td>
                  <td><?= $value['percent'] ?> %</td>
                  <?php if($value['date_end'] < (date('Y-m-d'))) { ?>
                  <td><span  class="badge badge-danger" style="font-size: 15px;">
                   Hết hạn
                 </span></td>
                 <?php } else { ?>
                 <td><span  class="badge badge-success" style="font-size: 15px;">
                   Có thể sử dụng
                 </span></td>
                 <?php } ?>
                 <td style="text-align: center;">
                   <button type="button" title="Chi tiết" class="btn btn-sm btn-outline-primary" 
                   data-toggle="modal" data-target="#edit<?= $value['id'] ?>" style="width: 30px;">
                   <i class="fas fa-info"></i>
                 </button> 
               </td>
             </tr>

             <?php $i++; ?>
           <?php endforeach ?>

         </tbody></table>
       </div>
       <!-- /.card-body -->
       <div class="card-footer clearfix">

         <span id="count" class="badge badge-danger" style="font-size: 15px;">
           <?= $i; ?> bản ghi / trang </span>
           <ul class="pagination pagination-sm m-0 float-right">
           </ul>
         </div>
       </div>

       <!-- end: table -->

     </form>

     <!-- /.card -->
   </div>
 </div><!-- /.row -->
</div><!-- /.container-fluid -->
</section>

</div>


<!--st: add modal -->
<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?= base_url() ?>Giamgia/add" method="post" id="insert_form">

          <div class="form-group">
            <label for="Phantram">Phần trăm giảm</label>
            <input type="number" class="form-control"
            name="Phantram" placeholder="Phần trăm giảm" id="Phantram" >
          </div>

          <div class="form-group">
            <label>Ngày hết hạn</label>

            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
              </div>
              <input type="date" class="form-control" name="Hethan" id="Hethan">

            </div>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="Submit_insert()">Lưu</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8" >

  function Submit_insert(argument) {

    var phantram = $('#Phantram').val().trim();
    var hethan = $('#Hethan').val().trim();

    if(phantram != '' && phantram > 0 && hethan != '')
    {
      $('#add_modal').modal('hide');
      $('#insert_form').submit();
    }

    else
    {

      if(phantram == '' || phantram <= 0 ){ $("#Phantram").addClass('is-invalid');}
      else { $("#Phantram").removeClass('is-invalid');}

      if(hethan == ''){ $("#Hethan").addClass('is-invalid');}
      else { $("#Hethan").removeClass('is-invalid');}
    }

  }
</script>

<!-- end: add_modal -->

<!-- st: multidel_modal -->

<div class="modal fade" id="multidel" 
tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel"></h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <p>Xóa các bản ghi được chọn ????</p>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-danger" name="btn_multidel" onclick="btn_multidelClick();">
      Xóa</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
    </div>

  </div>
</div>
</div>


<script type="text/javascript" charset="utf-8" >

  function btn_multidelClick() {

   $('#multidel').modal('hide');

   $('#myform').submit();

 }

</script>

<!-- end: multidel_modal -->


<!-- st: edit_modal -->

<?php foreach ($all as $value): ?>


  <div class="modal fade" id="edit<?= $value['id'] ?>" 
    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <form>

            <input type="hidden" name="id" value="<?= $value['id'] ?>">

            <div class="form-group">
              <label>Mã giảm giá</label>
              <input type="text" class="form-control" 
               value="<?= $value['id'] ?>" readonly>
            </div>

            <div class="form-group">
              <label>Phần trăm giảm (%)</label>
              <input type="text" class="form-control" 
              value="<?= $value['percent'] ?> %" readonly>
            </div>

            <div class="form-group">
              <label>Từ</label>
              <input type="text" class="form-control" 
              value="<?= $value['date_start'] ?>" readonly>
            </div>

            <div class="form-group">
              <label>Hết hạn</label>
              <input type="text" class="form-control" 
              value="<?= $value['date_end'] ?>" readonly>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          </div>

        </form>

      </div>
    </div>
  </div>

<?php endforeach ?>
<!-- end: edit_modal -->


<?php include 'footer.php'; ?>
