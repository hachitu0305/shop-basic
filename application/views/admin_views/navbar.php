<nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
      <a href="<?= base_url() ?>Trangchu" target="_blank" class="nav-link">Quản lý cửa hàng nội thất</a>
    </li>
  </ul>

  <!-- SEARCH FORM -->
  <form class="form-inline ml-3">
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" type="text" placeholder="Tìm kiếm"
      aria-label="Search" id="searchbox">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="button">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Messages Dropdown Menu -->
    <li class="nav-item">
      <a class="nav-link" href="<?= base_url() ?>Admin">
        <i class="fas fa-home" title="Trang chủ"></i>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="<?= base_url() ?>Admin/logout">
        <i class="fas fa-sign-out-alt" title="Đăng xuất"></i>
      </a>
    </li>

  </ul>
</nav>