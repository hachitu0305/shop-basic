<?php include 'header.php'; ?>

<body class="sidebar-mini" style="height: auto;" onload="loadBody()">
  <div class="wrapper">


    <?php include 'navbar.php'; ?>
    
    <?php include 'sidebar.php' ?>


    <div class="content-wrapper" style="min-height: 823.896px;">

     <?php include 'header_content.php'; ?>

     <section class="content">
      <div class="container-fluid">



      </div><!-- /.container-fluid -->
    </section>

    <script type="text/javascript" charset="utf-8" async defer>
      function loadBody() {
        var path='<?= base_url() ?>';
        var listMonth=[];
        var listSoluonghang=[]; 
        var listSoluongdon=[]; 
        $.ajax({
          url: path+'Thongke/getThongke', 
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          type: 'post',
          success: function (res) {
            var listBase=res[0].list;
            if(listBase){
              listBase.forEach(item => {
                listMonth.push(item.thang);
                listSoluonghang.push(item.soluongban);
                listSoluongdon.push(item.soluongdon);
              });
            }


            var areaChartData = {
              labels  : listMonth,
              datasets: [
              {
                label               : 'Số sản phẩm',
                backgroundColor     : '#007bff',
                borderColor         : '#007bff',
                pointRadius          : true,
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : listSoluonghang
              },
              {
                label               : 'Số đơn hàng',
                backgroundColor     : 'black',
                borderColor         : 'black',
                pointRadius         : true,
                pointColor          : '#3b8bba',
                pointStrokeColor    : '#c1c7d1',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(220,220,220,1)',
                data                : listSoluongdon
              },
              ]
            }

            var areaChartOptions = {
              maintainAspectRatio : false,
              responsive : true,
              legend: {
                display: false
              },
              scales: {
                xAxes: [{
                  gridLines : {
                    display : false,
                  }
                }],
                yAxes: [{
                  gridLines : {
                    display : false,
                  }
                }]
              }
            }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $('#visitors-chart').get(0).getContext('2d')
    var lineChartOptions = areaChartOptions
    var lineChartData = areaChartData
    lineChartData.datasets[0].fill = false;
    lineChartData.datasets[1].fill = false;
    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, { 
      type: 'line',
      data: lineChartData, 
      options: lineChartOptions
    })
  }
});

      }
    </script>

    <script type="text/javascript" defer async>
      $(function () {

    //     var areaChartData = {
    //       labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //       datasets: [
    //       {
    //         label               : 'Digital Goods',
    //         backgroundColor     : '#007bff',
    //         borderColor         : '#007bff',
    //         pointRadius          : true,
    //         pointColor          : '#3b8bba',
    //         pointStrokeColor    : 'rgba(60,141,188,1)',
    //         pointHighlightFill  : '#fff',
    //         pointHighlightStroke: 'rgba(60,141,188,1)',
    //         data                : [28, 48, 40, 19, 86, 27, 90]
    //       },
    //       {
    //         label               : 'Electronics',
    //         backgroundColor     : 'black',
    //         borderColor         : 'black',
    //         pointRadius         : true,
    //         pointColor          : '#3b8bba',
    //         pointStrokeColor    : '#c1c7d1',
    //         pointHighlightFill  : '#fff',
    //         pointHighlightStroke: 'rgba(220,220,220,1)',
    //         data                : [65, 59, 80, 81, 56, 55, 40]
    //       },
    //       ]
    //     }

    //     var areaChartOptions = {
    //       maintainAspectRatio : false,
    //       responsive : true,
    //       legend: {
    //         display: false
    //       },
    //       scales: {
    //         xAxes: [{
    //           gridLines : {
    //             display : false,
    //           }
    //         }],
    //         yAxes: [{
    //           gridLines : {
    //             display : false,
    //           }
    //         }]
    //       }
    //     }

    // //-------------
    // //- LINE CHART -
    // //--------------
    // var lineChartCanvas = $('#visitors-chart').get(0).getContext('2d')
    // var lineChartOptions = areaChartOptions
    // var lineChartData = areaChartData
    // lineChartData.datasets[0].fill = false;
    // lineChartData.datasets[1].fill = false;
    // lineChartOptions.datasetFill = false

    // var lineChart = new Chart(lineChartCanvas, { 
    //   type: 'line',
    //   data: lineChartData, 
    //   options: lineChartOptions
    // })
  })
</script>

</div>


<?php include 'footer.php'; ?>

