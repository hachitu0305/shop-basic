<?php include 'header.php' ?>

<?php include 'header_top.php' ?>
<?php include 'header_bottom.php' ?>

<!-- st: main -->

<div class="login">
	<div class="wrap">
		<div class="col_1_of_login span_1_of_login">
			<h4 class="title">Khách hàng mới</h4>
			<p>Tạo tài khoản nếu bạn chưa có tài khoản !!! &#160; &#160;
				<a href="<?= base_url() ?>Khachhang/load_register" 
					class="mybutton" style="width: auto;">Tạo tài khoản</a></p>
					<div class="clear"></div>
				</div>
				<div class="col_1_of_login span_1_of_login">
					<div class="login-title">
						<h4 class="title">Khách hàng đăng nhập</h4>
						<div id="loginbox" class="loginbox">
							<form action="<?= base_url() ?>Khachhang/logincustomer" method="post" 
								name="login" id="login-form">
								<fieldset class="input">
									<p id="login-form-username">
										<label for="modlgn_username">Email</label>
										<input id="modlgn_username" type="email" name="email" class="inputbox" 
										size="18" autocomplete="off">
									</p>
									<p id="login-form-password">
										<label for="modlgn_passwd">Mật khẩu</label>
										<input id="modlgn_passwd" type="password" name="password" class="inputbox" 
										size="18" autocomplete="off">
									</p>
									<div class="remember">
										<p id="login-form-remember">
											<label for="modlgn_remember"><a href="#">Quên mật khẩu ? </a></label>
										</p>
										<input type="submit" name="Submit" class="button" value="ĐĂNG NHẬP">
										<div class="clear">
										</div>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>


		<!-- end: main -->