<!-- start slider -->
<div id="fwslider">
    <div class="slider_container">
        <div class="slide"> 
            <!-- Slide image -->
            <img height="600px" src="<?= base_url() ?>/assets/uploads/banner001.jpg" alt=""/>
            <!-- /Slide image -->
            <!-- Texts container -->
            <div class="slide_content">
                <div class="slide_content_wrap">
                </div>
            </div>
            <!-- /Texts container -->
        </div>
        <!-- /Duplicate to create more slides -->
        <div class="slide">
            <img height="600px" src="<?= base_url() ?>/assets/uploads/banner002.jpg" alt=""/>
            <div class="slide_content">
                <div class="slide_content_wrap">
                </div>
            </div>
        </div>
        <!--/slide -->
        <!-- /Duplicate to create more slides -->
        <div class="slide">
            <img height="600px" src="<?= base_url() ?>/assets/uploads/banner003.jpg" alt=""/>
            <div class="slide_content">
                <div class="slide_content_wrap">
                </div>
            </div>
        </div>
        <!--/slide -->
    </div>
    <div class="timers"></div>
    <div class="slidePrev"><span></span></div>
    <div class="slideNext"><span></span></div>
</div>
<!--/slider -->
