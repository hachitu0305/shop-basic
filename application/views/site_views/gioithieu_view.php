<?php include 'header.php' ?>

<?php include 'header_top.php' ?>

<?php include 'header_bottom.php' ?>

<!-- st: main -->

<div class="">
	<div class="wrap" style="margin-bottom: 100px;">
		<h2 style="text-align: center;" class="head">GIỚI THIỆU CÔNG TY NỘI THẤT AMD VIỆT NAM</h2>
		<br>
		<p>
			Công ty nội thất AMD Việt Nam là công ty hoạt động trong lĩnh vực tư vấn, thiết kế, sản xuất nội thất đồ gỗ gia đình. Đi đầu trong lĩnh vực thiết kế nội thất gia đình, nội thất dự án chung cư cao cấp, nội thất dự án khu biệt thự nhà liền kề, nội thất khách sạn, các dự án về nội thất văn phòng…Để rút ngắn được quy trình hoàn thiện dự án cho khách hàng, giúp cho khách hàng sẽ có các sản phẩm nội thất gỗ với giá rẻ nhất, Công ty chúng tôi đã đầu tư rất mạnh vào hệ thống xưởng sản xuất. Với hệ thống máy móc hiện đại, quy trình sản xuất khép kín, những sản phẩm tại xưởng sản xuất làm ra để cung cấp cho khách hàng là những mẫu sản phẩm tốt nhất thị trường.
		</p>

		<br>

		<img src="<?= base_url() ?>/assets/site/images/gt1.jpg" style="margin-bottom: 20px;">



		<p>
			Xưởng sản xuất đồ gỗ của Công ty Nội thất AMD hiện nằm tại địa chỉ Số 135/12B đường Đại Linh- Trung Văn- Nam Từ Liêm- Hà Nội. Với diện tích nhà xưởng lên tới hơn 400m2, có phân khu sản xuất ra riêng biệt với khu cắt, đóng thô. Khu lắp giáp hoàn thiện công đoạn 2, phòng sơn cách biệt hoàn toàn với hệ thống sơn kín, có xử lý khí sơn trong quá trình sơn đồ gỗ nội thất. Tất cả đều được phân khu rõ ràng thuận lợi cho một dây truyền sản xuất khép kín từ nhập gỗ thô cho đến xuất xưởng lắp đặt tại dự án.
		</p>

		<br>

		<img src="<?= base_url() ?>/assets/site/images/gt2.jpg" style="margin-bottom: 20px;">


		<p>
			Tại địa điểm của xưởng gỗ hiện nay rất thuận tiện cho việc vận chuyển hàng hóa của công ty cũng như sản phẩm của khách hàng đi các vùng trong nội thành thành phố Hà Nôi. Thuộc địa phận phường Trung Văn-Nam Từ Liêm, có tuyến đường Lê Văn Lương kéo dài và tuyến đường trọng yếu Đại Lộ Thăng Long cách không xa, hàng hóa, sản phẩm của khách sẽ được vận chuyển đến nhà một cánh nhanh nhất, không có cản trở nào hết, thật tiện lợi đúng không nào.
		</p>

		<br>
		<img src="<?= base_url() ?>/assets/site/images/gt3.jpg" style="margin-bottom: 20px;">

		<p>
			Hiện tại về số lượng máy móc thiết bị tại xưởng đã có đủ 100% những thiết bị đủ tiêu chuẩn để có thể sản xuất đồ nội thất cho các dự án lớn nhỏ khác nhau. Những cỗ máy chế biến, sản xuất,lắp giáp có đụng chủ loại với những cỗ máy lên tới hàng trăm triệu đồng của Nhật Bản. Số lượng công nhân lành nghề của công ty hiện có tới hơn 20 người. Tất cả các công nhân kỹ thuật ở đây đều có tay nghề cao, các anh em công nhân đều có những niềm yêu nghề sâu đậm, tất cả đều đã làm tại xưởng từ rất lâu có những người thâm niên từ những năm đầu xưởng mới thành lập.
		</p>

		<br>

		<p>
			Để hiểu rõ về quy trình sản xuất của công ty chúng tôi mời quý khác hàng về tại địa chỉ xưởng mộc của chúng tôi. Tại đây bạn sẽ được tham quan, tìm hiểu về cách thức làm việc chuyên nghiệp trên những công nghệ hiện đại trong hệ thống xưởng mộc Công ty AMD Việt Nam.
		</p>
	</div>


	<!-- end: main -->


	<?php include 'footer.php' ?>