<?php include 'header.php'; ?>

<?php include 'header_top.php' ?>

<!-- st:main -->

<div class="mens">     
	<div class="main">
		<div class="wrap">

			<?php foreach ($detail as $value): ?>


				<div>
					<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>
								<a>
									<img class="etalage_thumb_image" 
									src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
									class="img-responsive" />
									<img class="etalage_source_image" 
									src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>" 
									class="img-responsive"
									title="" />
								</a>
							</li>
							<li>
								<img class="etalage_thumb_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive" />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive" 
								title="" />
							</li>
							<li>
								<img class="etalage_thumb_image"
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
							</li>
							<li>
								<img class="etalage_thumb_image"
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>" 
								class="img-responsive"  />
								<img class="etalage_source_image" 
								src="<?= base_url() ?>/assets/uploads/<?= $value['img_link'] ?>"
								class="img-responsive"  />
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="desc1 span_3_of_2">
						<h3 class="m_3" style="color: red; font-size: 30px;">
							Sản phẩm : <?= $value['name'] ?>
						</h3>
						
						<p class="m_5">Giá : 
							<?= str_replace(".00","",$this->cart->format_number(Cal_price($value['price'],$value['discount']))) ?> 
						VND</p>

						<form method="post" 
						action="<?= base_url() ?>Giohang/addto_cart/<?= $value['code'] ?>">
						<div style="display: inline-flex;">
							<div style="margin-top: 20px;margin-right: 10px">Số lượng: </div>
							<input type="number" name="qty" min="1" 
							class="form-control input-number" value="1">
						</div>
						<div class="btn_form">
							<input type="submit" value="Thêm vào giỏ" class="mybutton">
						</div>
					</form>
				</div>
				<div class="clear"></div>	

				<div class="clients">
					<h3 class="m_3">* Sản phẩm cùng loại</h3>
					<ul id="flexiselDemo3">

						<?php foreach ($same as $sa): ?>
							<li><img src="<?= base_url() ?>/assets/uploads/<?= $sa['img_link'] ?>" 
								style="width: 100px; height: 100px;"/>
								<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $sa['code'] ?>">
									<p>
										<?= $sa['code'] ?></p></a><p>
											<?= str_replace(".00","",$this->cart->format_number(Cal_price($sa['price'],$sa['discount']))) ?> đ
										</p></li>
									<?php endforeach ?>

								</ul>
								<script type="text/javascript">
									$(window).load(function() {
										$("#flexiselDemo1").flexisel();
										$("#flexiselDemo2").flexisel({
											enableResponsiveBreakpoints: true,
											responsiveBreakpoints: { 
												portrait: { 
													changePoint:480,
													visibleItems: 1
												}, 
												landscape: { 
													changePoint:640,
													visibleItems: 2
												},
												tablet: { 
													changePoint:768,
													visibleItems: 3
												}
											}
										});

										$("#flexiselDemo3").flexisel({
											visibleItems: 5,
											animationSpeed: 1000,
											autoPlay: false,
											autoPlaySpeed: 3000,    		
											pauseOnHover: true,
											enableResponsiveBreakpoints: true,
											responsiveBreakpoints: { 
												portrait: { 
													changePoint:480,
													visibleItems: 1
												}, 
												landscape: { 
													changePoint:640,
													visibleItems: 2
												},
												tablet: { 
													changePoint:768,
													visibleItems: 3
												}
											}
										});

									});
								</script>
								<script type="text/javascript"
								src="<?= base_url() ?>/assets/site/js/jquery.flexisel.js">
							</script>
						</div>

						<div class="toogle">
							<h3 class="m_3">* Chi tiết sản phẩm</h3>
							<p class="m_text">
								<p style="color: red;">* Thiết kế : <?= $value['com_name'] ?></p>
								<br>
								<p style="color: red;">* Lượt xem : <?= $value['count_view'] ?></p>
								<br>
								<?= $value['detail'] ?>
							</p>
						</div>
					</div>

				<?php endforeach ?>

				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<?php 

	function Cal_price( $price , $discount)
	{
		$res = round($price - ( ($discount * $price)/100 ));

		return $res;

	}

	?>


	<!-- end:main -->