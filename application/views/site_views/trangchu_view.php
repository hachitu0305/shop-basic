<?php include 'header.php' ?>

<?php include 'header_top.php' ?>

<!-- st:main -->

<div class="main">
	<div class="wrap">
		<div class="section group">
			<div>

				<!-- st:Ban chay -->
				<h2 class="head">Bán chạy</h2>

				<?php foreach ($itemshot as $ithot): ?>

					<div class="col_1_of_3 span_1_of_3">
						<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $ithot['code'] ?>">
							<div class="inner_content clearfix">
								<div class="product_image">
									<img src="<?= base_url() ?>/assets/uploads/<?= $ithot['img_link'] ?>" 
									alt="@@@@" style="width: 250px; height: 250px;"/>
								</div>
								<div class="price">
									<div class="cart-left">
										<p class="title"><?= $ithot['name'] ?></p>
										<div class="price1" style="font-size: 15px;">
											<span class="actual">
												<?php echo str_replace(".00","",$this->cart->format_number(round($ithot['price']))) ?>VND
											</span>
										</div>
									</div>
									<div class="cart-right"> </div>
									<div class="clear"></div>
								</div>				
							</div>
						</a>
					</div>

				<?php endforeach ?>
				

				<div class="clear"></div>

				<!-- end: Ban chay -->

				<!-- st: Khuyen mai -->

				<h2 class="head">Khuyến mãi</h2>

				<?php foreach ($itemssale as $itsale): ?>

					<div class="col_1_of_3 span_1_of_3">
						<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $itsale['code'] ?>">
							<div class="inner_content clearfix">
								<div class="product_image">
									<img src="<?= base_url() ?>/assets/uploads/<?= $itsale['img_link'] ?>" 
									alt="@@@@" style="width: 250px; height: 250px;"/>
								</div>
								<div class="sale-box1"><span class="on_sale title_shop">Sale</span></div>	
								<div class="price">
									<div class="cart-left">
										<p class="title"><?= $itsale['name'] ?></p>
										<div class="price1" style="font-size: 15px;">
											<span class="reducedfrom">
												<?php echo(round($itsale['price'])) ?>VND</span>
												<span class="actual">
													<?php 
													str_replace(".00","",Cal_price($itsale['price'] , $itsale['discount'])); 
													?>VND
												</span>
											</div>
										</div>
										<div class="cart-right"></div>
										<div class="clear"></div>
									</div>				
								</div>
							</a>
						</div>

					<?php endforeach ?>

					<div class="clear"></div>

					<!-- end: Khuyen mai -->

					<!-- st: san pham moi -->

					<h2 class="head">Sản phẩm mới</h2>	

					<?php foreach ($itemsnew as $itnew): ?>

						<div class="col_1_of_3 span_1_of_3">
							<a href="<?= base_url() ?>Trangsanpham/getdetail/<?= $itnew['code'] ?>">
								<div class="inner_content clearfix">
									<div class="product_image">
										<img src="<?= base_url() ?>/assets/uploads/<?= $itnew['img_link'] ?>"
										alt="@@@@" style="width: 250px; height: 250px;"/>
									</div>
									<div class="sale-box"><span class="on_sale title_shop">New</span></div>	
									<div class="price">
										<div class="cart-left">
											<p class="title"><?= $itnew['name'] ?></p>
											<div class="price1" style="font-size: 15px;">
												<span class="actual">
													<?php echo str_replace(".00","",$this->cart->format_number(round($itnew['price']))) ?>VND
												</span>
											</div>
										</div>
										<div class="cart-right"> </div>
										<div class="clear"></div>
									</div>				
								</div>
							</a>
						</div>

					<?php endforeach ?>

					<div class="clear"></div>

					<!--end: san pham moi  -->


				</div>
				
			</div>
		</div>
	</div>

	<?php 

	function Cal_price( $price , $discount)
	{
		$res = round($price - ( ($discount * $price)/100 ));

		echo $res;
	}

	?>

	<!-- end:main -->