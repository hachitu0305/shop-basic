<?php include 'header.php' ?>

<?php include 'header_top.php' ?>


<?php include 'header_bottom.php' ?>

<?php include 'slide.php' ?>

<!-- st:main -->

<div class="main">
	<div class="wrap">
		<div class="section group">
			<div class="cont span_2_of_3">
				<h2 class="head">Tin tức nổi bật</h2>

				<div class="mens-toolbar">
					<div class="pager">
						<ul class="dc_pagination dc_paginationA dc_paginationA06" 
						style="margin-left: 100px;">
						<?= $page ?>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>

				<?php foreach ($all as $value): ?>
					<div class="news col_1_of_3 span_1_of_3">
						<a href="<?= base_url() ?>Trangtintuc/getDetail/<?= $value['id'] ?>">
							<div class="inner_content clearfix">
								<div class="product_image">
									<img src="<?= base_url() ?>/assets/uploads/<?= $value['banner1'] ?>" 
									alt="#" style="width: 250px; height: 250px;"/>
								</div>
								<div class="t-new" style="margin-top: 10px">
									<p class="title"><?= $value['title'] ?></p>
								</div>	
								<p style="color: gray; font-size: 10px;margin: 5% 0% 5% 60%;"><?= $value['created'] ?></p>			
							</div>
						</a>
					</div>	
				<?php endforeach ?>

			</div>

			<div class="rsidebar span_1_of_left">
				<div class="top-border"> </div>
				<div class="border">
					<link href="<?= base_url() ?>assets/site/css/default.css" 
					rel="stylesheet" type="text/css" media="all" />
					<link href="<?= base_url() ?>assets/site/css/nivo-slider.css" 
					rel="stylesheet" type="text/css" media="all" />
					<script src="<?= base_url() ?>assets/site/js/jquery.nivo.slider.js"></script>
					<script type="text/javascript">
						$(window).load(function() {
							$('#slider').nivoSlider();
						});
					</script>
					<div class="slider-wrapper theme-default">
						<div id="slider" class="nivoSlider">
							<img src="<?= base_url() ?>/assets/uploads/noithat1.jpg" />
							<img src="<?= base_url() ?>/assets/uploads/imagesq.jpg" />
							<img src="<?= base_url() ?>/assets/uploads/noithat3.jpg" />
						</div>
					</div>

				</div>
				<div class="top-border"> </div>
				<div class="sidebar-bottom">
					<h2 class="m_1">Tin mới<br></h2>
					<p class="m_text">Đăng ký để nhận thông báo</p>
					<img src="<?= base_url() ?>assets/uploads/noithat3.jpg" alt="@@@@">
					<div style="height: 20px;"></div>
					<div class="subscribe">
						<form>
							<a class="mybutton" style="width: auto; background: #20c997;"
							href="<?= base_url() ?>Khachhang/load_register">Đăng ký</a>
						</form>
					</div>
				</div>
			</div>

			<div class="clear"></div>
		</div>
	</div>
</div>

<?php 

function Cal_price( $price , $discount)
{
	$res = round($price - ( ($discount * $price)/100 ));

	echo $res;
}

?>

<!-- end:main -->


<?php include 'footer.php' ?>