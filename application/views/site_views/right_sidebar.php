<form>
	<div class="rsingle span_1_of_single" style="margin-top: 30px; ">
		<section class="sky-form" style="min-height: 300px; max-height: auto;">
			<h5 class="m_1">Danh mục nổi bật</h5>
			<div class="col col-4">

				<?php foreach ($cats as $cat): ?>
					<label class="checkbox">
						<input type="checkbox" name="cat" value="<?= $cat['id'] ?>" >
						<i></i><?= $cat['name'] ?>
					</label>
				<?php endforeach ?>

			</div>
		</section>

		<div style="height: 30px;"></div>

		<section class="sky-form" style="min-height: 300px; max-height: auto;">
			<h5 class="m_1">Loại thiết kế</h5>

			<div class="col col-4" style="border-style: solid;">
				<?php foreach ($coms as $com): ?>

					<label class="checkbox"><input type="checkbox" name="com" 
						value="<?= $com['id'] ?>" >
						<i></i><?= $com['name'] ?>
					</label>

				<?php endforeach ?>
			</div>

		</section>

		<div style="height: 30px;"></div>

		<section>
			<button type="button" class="mybutton" onclick="btn_clk();">Tìm theo bộ lọc</button>
		</section>

		<script src="<?= base_url() ?>assets/site/js/jquery.easydropdown.js"></script>
	</div>
</form>


<script  type="text/javascript" charset="utf-8" >
	
	function btn_clk(argument) {

		var path = '<?= base_url() ?>';

		var cat = [];

		$("input[name = 'cat']:checked").each(function(index, el) {

			cat.push(this.value);
		});

		var com = [];

		$("input[name = 'com']:checked").each(function(index, el) {

			com.push(this.value);
		});


    	$.ajax({
    		url: path+'Trangsanpham/filter',
    		type: 'POST',
    		dataType: 'html',
    		data: {cat: cat,com: com},
    	})
    	.done(function() {
    		console.log("success");
    	})
    	.fail(function() {
    		console.log("error");
    	})
    	.always(function(res) {
    		
    		$('#content').html('');
    		$('#content').html(res);

    	});
    	
    }

</script>
