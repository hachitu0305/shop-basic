<?php include 'header.php' ?>

<?php include 'header_top.php' ?>

<?php include 'header_bottom.php' ?>


<style>
#mytable {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#mytable td, #mytable th {
	border: 1px solid #ddd;
	padding: 8px;
}

#mytable tr:nth-child(even){background-color: #f2f2f2;}

#mytable tr:hover {background-color: #ddd;}

#mytable th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #28a745;;
	color: white;
}

</style>

<!-- st: main -->

<div class="register_account">
	<div class="wrap">

		<?php if(count($this->cart->contents()) != 0)  { ?>

		<table id="mytable" style="height: auto; ">
			<tr>
				<th>Hình ảnh</th>
				<th>Mã sản phẩm</th>
				<th>Tên sản phẩm</th>
				<th>Số lượng</th>
				<th>Đơn giá</th>
				<th>Thành tiền</th>
				<th>Tác vụ</th>
			</tr>
			<?php $i = 1; ?>
			<?php foreach ($this->cart->contents() as $item): ?>
				<form action="<?= base_url() ?>Giohang/update_cart/<?= $item['id'] ?>" method="post">
					<tr>
						<td style="width: 200px; height: 100px; text-align: center;">
							<img src="<?= base_url() ?>assets/uploads/<?= $item['img'] ?>" 
							alt="@@@@"
							style="width: 100px;height: 100px;">
						</td>
						<td style="width: 150px; text-align: center; vertical-align: center;">
							<?= $item['id'] ?>
						</td>
						<td style="width: 150px; text-align: center; vertical-align: center;">
							<?= $item['name'] ?>
						</td>
						<td style="width: 100px; height: auto ; text-align: center;">
							<input type="number" name="num-order" value="<?= $item['qty'] ?>" 
							style="width: 50px;" class="inputbox">
						</td>
						<td style="width: 150px; text-align: center; vertical-align: center;">
							<?php echo str_replace(".00","",$this->cart->format_number(round($item['price']))); ?>VND
						</td>
						<td style="width: 150px; text-align: center; vertical-align: center;">
							<?php echo str_replace(".00","",$this->cart->format_number(round($item['subtotal']))); ?>VND
						</td>
						<td style="width: 150px; text-align: center; vertical-align: center;">
							<input type="submit" value="Cập nhật giỏ" class="button">
							<input type="hidden" value="<?= $item['rowid'] ?>" name="num-id">
						</td>
					</tr>

				</form>

				<?php $i++; ?>

			<?php endforeach ?>
		</table>

		

		<div class="btn_form"> 

			<form>

				<a href="<?= base_url() ?>Giohang/removeall" 
					style="background: red;">xóa giỏ hàng</a>
					<a href="<?= base_url() ?>Thanhtoan">thanh toán</a>
					<p style="display: inline; text-align: right; 
					margin-left: 500px; color: red;">
					<span> TỔNG TIỀN : 
						<?php echo $this->cart->format_number($this->cart->total()); ?>đ</span>
					</p>
				</form>

			</div>

			<?php } else { ?>

			<h4 class="title">Không có sản phẩm nào trong giỏ</h4>
			<a href="<?= base_url() ?>Trangsanpham" style="color: red;">Tiếp tục mua hàng</a>
			
			<?php }  ?>
		</div>
	</div>


	<!-- end: main -->