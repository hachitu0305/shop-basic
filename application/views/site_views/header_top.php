<div class="header-top mb-5">
	<div class="wrap"> 
		<div class="cssmenu">
			<ul>
				<li><a href="<?= base_url() ?>Trangchu">Trang chủ</a></li> |
				<!-- <li><a href="<?= base_url() ?>Tranggioithieu">Giới thiệu</a></li> | -->
				<!-- <li><a href="<?= base_url() ?>Trangsanpham">Sản phẩm</a></li> | -->
				<!-- <li><a href="<?= base_url() ?>Trangtintuc">Tin tức</a></li> | -->
				<li><a href="<?= base_url() ?>Giohang">Giỏ hàng</a></li> |
				<li><a href="<?= base_url() ?>Thanhtoan">Thanh toán</a></li> |
				<?php if(empty($_SESSION['customer'])) { ?>
				<li><a href="<?= base_url() ?>Khachhang/load_logincustomer">Đăng nhập</a></li> |
				<li><a href="<?= base_url() ?>Khachhang/load_register">Đăng ký</a></li>
				<?php } else { ?>
				<li><a href="<?= base_url() ?>Khachhang/logoutcustomer">Đăng xuất</a></li> |
				<li style="width: auto;"><?= $_SESSION['customer'] ?></li>
				<?php } ?>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</div>
