<!DOCTYPE html>
<html>
<head>
	<title>Shop đồ nội thất</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link href="<?= base_url() ?>/assets/site/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?= base_url() ?>/assets/site/css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?= base_url() ?>/assets/site/css/form.css" rel="stylesheet" type="text/css" media="all" />

	<script type="text/javascript" src="<?= base_url() ?>/assets/site/js/jquery1.min.js"></script>
	<!-- start menu -->
	<link href="<?= base_url() ?>/assets/site/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
	<script type="text/javascript" src="<?= base_url() ?>/assets/site/js/megamenu.js"></script>
	<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
	<!--start slider -->
	<link rel="stylesheet" href="<?= base_url() ?>/assets/site/css/fwslider.css" media="all">

	<link rel="icon" href="https://icons-for-free.com/iconfiles/png/512/market+open+shop+shopping+store+icon-1320184216006471806.png">

	<script src="<?= base_url() ?>/assets/site/js/jquery-ui.min.js"></script>
	<script src="<?= base_url() ?>/assets/site/js/css3-mediaqueries.js"></script>
	<script src="<?= base_url() ?>/assets/site/js/fwslider.js"></script>
	<!--end slider -->
	<script src="<?= base_url() ?>/assets/site/js/jquery.easydropdown.js"></script>

	<!-- start details -->
	<script src="<?= base_url() ?>assets/site/js/slides.min.jquery.js"></script>
	
	<script>
		$(function(){
			$('#products').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				effect: 'slide, fade',
				crossfade: true,
				slideSpeed: 350,
				fadeSpeed: 500,
				generateNextPrev: true,
				generatePagination: false
			});
		});
	</script>
	<link rel="stylesheet" href="<?= base_url() ?>assets/site/css/etalage.css">
	<script src="<?= base_url() ?>assets/site/js/jquery.etalage.min.js"></script>
	<script>
		jQuery(document).ready(function($){

			$('#etalage').etalage({
				thumb_image_width: 360,
				thumb_image_height: 360,
				source_image_width: 900,
				source_image_height: 900,
				show_hint: true,
				click_callback: function(image_anchor, instance_id){
					alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
				}
			});

		});
	</script>	
	<!-- end: detail -->

</head>

<body>
	
