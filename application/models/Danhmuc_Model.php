<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Danhmuc_Model extends CI_Model {

    /**
     * @name string TABLE_NAME Holds the name of the table in use by this model
     */
    const TABLE_NAME = 'catalog';

    /**
     * @name string PRI_INDEX Holds the name of the tables' primary index used in this model
     */
    const PRI_INDEX = 'id';

    /**
     * Retrieves record(s) from the database
     *
     * @param mixed $where Optional. Retrieves only the records matching given criteria, or all records if not given.
     *                      If associative array is given, it should fit field_name=>value pattern.
     *                      If string, value will be used to match against PRI_INDEX
     * @return mixed Single record if ID is given, or array of results
     */
    public function get($where = NULL) {
    	$this->db->select(self::TABLE_NAME.'.*, parent.id as pa_id, parent.name as pa_name');
        $this->db->join('catalog parent', self::TABLE_NAME.'.parent_id = parent.id', 'left');

        $this->db->from(self::TABLE_NAME);
        if ($where !== NULL) {
          if (is_array($where)) {
           foreach ($where as $field=>$value) {
            $this->db->where($field, $value);
        }
    } else {
       $this->db->where(self::PRI_INDEX, $where);
   }
}
$result = $this->db->get()->result_array();
if ($result) {
  if ($where !== NULL) {
   return array_shift($result);
} else {
   return $result;
}
} else {
  return $result;
}
}

public function get_catalog_parent()
{
    $this->db->where(self::TABLE_NAME.'.parent_id', 0);
    return $this->db->get(self::TABLE_NAME)->result_array();
}



public function getLimit($limit, $offset)
{
    $this->db->select(self::TABLE_NAME.'.*, parent.id as pa_id, parent.name as pa_name');
    $this->db->join('catalog parent', self::TABLE_NAME.'.parent_id = parent.id', 'left');
    $this->db->order_by('created', 'desc');
    $data = $this->db->get(self::TABLE_NAME, $limit, $offset);
    $data = $data->result_array();
    return $data;

}


    /**
     * Inserts new data into database
     *
     * @param Array $data Associative array with field_name=>value pattern to be inserted into database
     * @return mixed Inserted row ID, or false if error occured
     */
    public function insert(Array $data) {
    	if ($this->db->insert(self::TABLE_NAME, $data)) {
    		return $this->db->insert_id();
    	} else {
    		return false;
    	}
    }

    /**
     * Updates selected record in the database
     *
     * @param Array $data Associative array field_name=>value to be updated
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of affected rows by the update query
     */
    public function update(Array $data, $where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->update(self::TABLE_NAME, $data, $where);
    	return $this->db->affected_rows();
    }

    /**
     * Deletes specified record from the database
     *
     * @param Array $where Optional. Associative array field_name=>value, for where condition. If specified, $id is not used
     * @return int Number of rows affected by the delete query
     */
    public function delete($where = array()) {
    	if (!is_array($where)) {
    		$where = array(self::PRI_INDEX => $where);
    	}
    	$this->db->delete(self::TABLE_NAME, $where);
    	return $this->db->affected_rows();
    }

    public function multidelete($id) {
        if(is_array($id)){
            $this->db->where_in('id', $id);
        }else{
            $this->db->where('id', $id);
        }
        $delete = $this->db->delete(self::TABLE_NAME);
        return $delete?true:false;
    }

    public function filter_parentcat($id)
    {
        $this->db->where(self::TABLE_NAME.'.parent_id', $id);
        $this->db->select(self::TABLE_NAME.'.*, parent.id as pa_id, parent.name as pa_name');
        $this->db->join('catalog parent', self::TABLE_NAME.'.parent_id = parent.id', 'left');
        $data = $this->db->get(self::TABLE_NAME)->result_array();


        $content = '';

        $content .='<thead>';
        $content .='<th style="width: 10px">Chọn</th>';
        $content .='<th>Tên danh mục</th>';
        $content .='<th>Danh mục cha</th>';
        $content .='<th style="width: auto;">Tác vụ</th>';
        $content .='</thead>';
        $content.='<tbody id="mytable">';

        foreach ($data as $value) {

            $content .='<tr>';
            $content .=' <td align="center">';
            $content .=' <label class="container-check">';
            $content .=' <input type="checkbox"  name="checked_id[]" value="'.$value['id'].'">';
            $content .=' <span class="checkmark"></span>';
            $content .=' </label>';
            $content .='</td>';
            $content .=' <td>'.$value['name'].'</td>';

            if (is_null($value['pa_name'])) {

                $content.='<td style="color: red;">'.'Không có danh mục cha'.'</td>';

            } 
            else {

              $content.='<td>'.$value['pa_name'].'</td>';
          }

          $content .=' <td>';
          $content .=' <button type="button" title="Chi tiết" class="btn btn-sm btn-outline-info" ';
          $content .='  data-toggle="modal" data-target="#edit'.$value['id'].'">';
          $content .='  <i class="fas fa-pen"></i>';
          $content .='  </button> ';
          $content .='  | <button title="Xóa" type="button" class="btn btn-sm btn-outline-danger"';
          $content .='  data-toggle="modal" data-target="#del'.$value['id'].'">';
          $content .='  <i class="fas fa-trash-alt"></i>';
          $content .='  </button>';
          $content .='</td>';
          $content .='</tr>';
      }

      $content .='</tbody>';

      return $content;

  }

}
?>