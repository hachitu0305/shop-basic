<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Thongke_Model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getCountNewOrders()
	{
		$this->db->where('date(created)=date(NOW())');
		return $this->db->get('transactions')->result_array();
	}

	public function getCountNewUsers()
	{
		$this->db->where('date(created)=date(NOW())');
		return $this->db->get('users')->result_array();
	}

	public function getThongke()
	{
		$str='select ifnull(b.soluongdon, 0) as soluongdon, ifnull(b.soluongban, 0) as soluongban ,a.thang from  (
			select "January" as thang
			union
			select "February" as thang
			union
			select "March" as thang
			union
			select "April" as thang
			union
			select "May" as thang
			union
			select "June" as thang
			union
			select "July" as thang
			union
			select "August" as thang
			union
			select "September" as thang
			union
			select "October" as thang
			union
			select "November" as thang
			union
			select "December" as thang
		) as a
		left join
		((select count(*) as soluongdon,
		(select sum(qty) from transactions_detail as td 
		where (select month(created) from transactions as t2 where t2.id=td.transactions_id)
		= month(t1.created)
	) as soluongban,
	(case month(created)
			when 1 then "January"
			when 2 then "February"
			when 3 then "March"
			when 4 then "April"
			when 5 then "May"
			when 6 then "June"
			when 7 then "July"
			when 8 then "August"
			when 9 then "September"
			when 10 then "October"
			when 11 then "November"
			when 12 then "December"
	end) as thang
	from transactions as t1 where year(created)=year(NOW())
	group by 
	(case month(created)
			when 1 then "January"
			when 2 then "February"
			when 3 then "March"
			when 4 then "April"
			when 5 then "May"
			when 6 then "June"
			when 7 then "July"
			when 8 then "August"
			when 9 then "September"
			when 10 then "October"
			when 11 then "November"
			when 12 then "December"
		end)
			order by created
		)) as b
		on a.thang=b.thang';
		$rs = $this->db->query($str);
		return $rs->result_array();	
    }

}

/* End of file Thongke_Model.php */
/* Location: ./application/models/Thongke_Model.php */